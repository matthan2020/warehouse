﻿

var addPalletController = function () {
    var checkxoa = '';
    var checkxoapallet = '';
    var checkxoaproduct = '';
    var datatest = [];
    var data = [];
    var data1 = [];
    var data2 = [];
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //$('#txtCreateDate').datepicker({
        //    autoclose: true,
        //    format: 'dd/mm/yyyy'
        //});;
       
        //loadData();
        $('.dateTime').datepicker({
            //autoclose: true,
            format: 'dd/mm/yyyy'
        });
        
       
        loadData();
        loadDataGoods();
        loadDataPallet();
        var sosanh = $('#hidIdM1').val();
        if (sosanh == 'Add') {
            
            $('#btnSaveGoods').prop('hidden', true);
            $('.anhien').prop('disabled', true);
            $('#checkthemsua').prop('hidden', true); 
            $('.ckcheck').prop('disabled', true);
            
        }
        else {
           
        }
        registerEvents();
        registerControls();
        //$(".nav-tabs a").click(function () {
        //    $(this).tab('show');
        //});

        
    }

    var registerEvents = function () {
       
       
       
        
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
       
        $('#ddlShowPagePallet').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadDataPallet(true);
        });

        $('#btnAddPallet').on('click', function () {
            $('#frmPallet').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    ddlCodePallet: {
                        required: true
                    },
                }
            });
            var sosanhpallet = $('#hidIdpallet').val();
            var checktrung = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].MPG_PRODUCT == $('#ddlCodeProduct').val()) {
                    checktrung = 1;
                   

                        if (sosanhpallet == '0') {
                            if (data[i].MPG_NUMBER >= Number(data[i].MPG_REALITY) + Number($('#txtNumberPallet').val())) {
                                AddPallet();
                            }
                            else {
                                common.confirm('Số lượng thực tế lớn hơn số lượng kế hoạch. Bạn có muốn lưu không?', function () {
                                    
                                        AddPallet();
                                    
                                });
                            }
                           
                        }
                        else {
                            if (data[i].MPG_NUMBER >= Number(data[i].MPG_REALITY) - Number($('#hidSoLuongCu').val()) + Number($('#txtNumberPallet').val())) {
                                UpdatePallet();
                            }
                            else {
                                common.confirm('Số lượng thực tế lớn hơn số lượng kế hoạch. Bạn có muốn lưu không?', function () {

                                    UpdatePallet();

                                });
                            }
                           
                        }
                   
                }
            }
            //if (checktrung == 0) {
            //    if (sosanhpallet == '0') {

            //        AddPallet();
            //    }
            //    else {
            //        UpdatePallet();
            //    }
            //}



        });

        $("#btnHuy").on('click', function () {
            window.location.href = ("../ImportGoods/Index");

        });
        $('#btnDeletePallet').on('click', function () {
            if (checkxoapallet == null || checkxoapallet == "" || checkxoapallet == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!", "error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeletePallet();
                });
            }

        });
     

       
        $('#btnSaveGoods').on('click', function () {
            $('#frmGoods').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    fileInputExcel: {
                        required: false
                    },
                    txtDes: {
                        required: false
                    },
                    txtLicenseName: {
                        required: false
                    },
                }
            });
            UpdateImportGoods();
        });

        $('#btnDeleteProduct').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!", "error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteProduct();
                });
            }
          
        });
        $('#btnAddProduct').on('click', function () {
            resetFormMainProduct();



        });

        $('#btnSavePallet').on('click', function () {
            resetFormMaintainance();



        });

        $('#btnSaveProduct').on('click', function () {
            $('#frmProduct').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtNumberTT: {
                        required: false
                    },
                }
            });
            var checkupdate = $('#hidIdproduct').val();
            if (checkupdate == '0') {
                SaveMapProduct();
            }
            else {
                UpdateMapProduct();
            }
            
        });
        $('body').on('click', '.update-product', function (e) {

            $('#hidIdproduct').val(1);
            var that = $(this).data('id');
            var codeProducct = $(this).data('product');
            e.preventDefault();
            GetMapProducttById(that, codeProducct);
        });

        $('body').on('click', '.update-pallet', function (e) {
            var tabel = document.getElementById('ddlCodePallet');
            tabel.remove(tabel.length-1);
            var thatpallet = $(this).data('pallet');
            $("#ddlCodePallet").append(new Option(thatpallet, thatpallet ));
           
            
            
            console.log(tabel);
            $('#hidIdpallet').val(1);
            var that = $(this).data('id');
            e.preventDefault();
            GetMapPalletById(that);
        });
       
      
      
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ",";
                        
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                        checkxoaproduct += "," + $(this).data('product') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                    checkxoaproduct = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkxoaproduct = checkxoaproduct.replace(/,,/g, ",");
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                    checkxoaproduct += "," + $(this).data('product') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                var chuoixoaproduct = "," + $(this).data('id') + ",";

                checkxoa = checkxoa.replace(chuoixoa, ",");
                checkxoaproduct = checkxoaproduct.replace(chuoixoaproduct, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkxoaproduct = checkxoaproduct.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
                checkxoaproduct = "";
            }
        });

        $('.ckcheck3').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoapallet.includes(chuoi) == true) {

                    }
                    else {
                        checkxoapallet += "," + $(this).data('id') + ",";
                    }
                    // checkxoa += "," + $(this).data('id') + ",";
                });

            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoapallet = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoapallet = checkxoapallet.replace(/,,/g, ",");
        });

        $('body').on('change', '.ckcheck2', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = $(this).data('id'); 
                if (checkxoapallet.includes(chuoi) == true) {

                }
                else {
                    checkxoapallet += "," + $(this).data('id') + ",";
                }

            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoapallet = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoapallet = checkxoapallet.replace(/,,/g, ",");
            if (checkxoapallet == ",") {
                checkxoapallet = "";
            }
        });

       

      

      
    }
    var registerControls = function () {
        dropdown.GetProduct($('#ddlCode'), '');
        dropdown.GetProductByGoods($('#ddlCodeProduct'), $('#hidIdM').val(), '');
        dropdown.GetPallet($('#ddlCodePallet'), '');
        
       // dropdown.GetProductType($('#ddlProductType'), '');
    }
    
    function loadData(isPageChanged) {

        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-pallet').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                idGoods: $('#hidIdM').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../ImportGoods/GetAllMapGoodsProduct',
            dataType: 'json',
            success: function (response) {
                if (response.Data.TotalRow > 0) {
                    data = response.Data.Items;
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: i + 1,
                            MPG_GOODS: item.MPG_GOODS,
                            MPG_PRODUCT: item.MPG_PRODUCT,
                            MPG_NUMBER: item.MPG_NUMBER,
                            MPG_REALITY: item.MPG_REALITY,
                            ShowView: $('#hidIdM1').val()=='Add'?'d-none':'',
                        });
                      
                        
                    });
                    if (render != '') {
                        $('#tbl-pallet').html(render);
                    }
                    common.wrapPaging(response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="6">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-pallet').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllMapGoodsProduct', 'error');
            }
        });
    }

    function loadDataGoods() {
        $.ajax({
            type: 'GET',
            data: {
                idGoods: $('#hidIdM').val()
            },
            url: '../ImportGoods/GetAllGoodsById',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                if (response.Success == true) {
                    var data = response.Data.Items[0];
                    $('#txtLicensePlates').val(data.IG_LICENSE_PLATES);
                    $('#txtLicenseName').val(data.IG_LICENSE_NAME);
                    $('#txtFromDate').val(data.IG_GOODS_DATE);
                    $('#txtDes').val(data.IG_DESCRIPTION);
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function GetAllGoodsById', 'error');
            }
        });
    }
    function loadDataPallet(isPageChanged) {

        var template = $('#table-template-pallet-add').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                idGoods: $('#hidIdM').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../ImportGoods/GetAllMapGoodsPallet',
            dataType: 'json',
            success: function (response) {
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: i + 1,
                            MPP_ID: item.MPP_ID,
                            MPP_PALLET: item.MPP_PALLET,
                            MPP_NUMBER: item.MPP_NUMBER,
                            MPP_PRODUCT: item.MPP_PRODUCT,
                            MPP_GOODS: item.MPP_GOODS
                        });

                       
                    });
                    if (render != '') {
                        $('#tbl-pallet-add').html(render);
                    }
                    common.wrapPaging1('#paginationULPallet', response.Data.TotalRow, function () {
                        loadDataPallet(false);
                    }, isPageChanged);
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="5">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-pallet-add').html(render);
                }
                $('#lblTotalRecordsPallet').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllMapGoodsProduct', 'error');
            }
        });
    }
    function resetFormMaintainance() {
        $('#hidIdpallet').val(0);
        //$('#ddlProductType').val('');
       // initTreeDropDownCategory('');
        dropdown.GetPallet($('#ddlCodePallet'), '');
        //$('#ddlCodePallet').change('');
        $('#ddlCodeProduct').val('');
        
        $('#txtNumberPallet').val('');
       

    }

    function resetFormMainProduct() {
        $('#hidIdproduct').val(0);
        //$('#hidIdM').val(0);
        //$('#ddlProductType').val('');
        // initTreeDropDownCategory('');
        //$('#ddlCodePallet').change('');
        $('#ddlCode').val('');

        $('#txtNumber').val('');
        $('#txtNumberTT').val('');


    }

    function AddPallet() {
        if ($('#frmPallet').valid()) {
               
                $.ajax({
                    type: "POST",
                    url: "../ImportGoods/SavePalletProduct",
                    data: {
                        id: $('#hidIdM').val(),
                        CodePallet: $('#ddlCodePallet').val(),
                        CodeProduct: $('#ddlCodeProduct').val(),
                        NumberPallet: $('#txtNumberPallet').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            resetFormMaintainance();

                            common.stopLoading();
                            loadDataPallet(true);
                            loadData(true);
                        }
                        else {
                                common.notify(response.Message, 'error');
                        }

                    },
                    error: function () {
                        common.notify('Can not load ajax function SavePalletProduct', 'error');
                        common.stopLoading();
                    }
                }); 
        }
    }

    function UpdatePallet() {
        if ($('#frmPallet').valid()) {

            $.ajax({
                type: "POST",
                url: "../ImportGoods/UpdateMapPallet",
                data: {
                    id: $('#hidIdM').val(),
                    idPallet: $('#hidIdpalletCu').val(),
                    CodePallet: $('#ddlCodePallet').val(),
                    CodeProduct: $('#ddlCodeProduct').val(),
                    NumberPallet: $('#txtNumberPallet').val(),
                    checkcapnhat: $('#hidIdM1').val(),
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Cập nhật thành công', 'success');

                        resetFormMaintainance();

                        common.stopLoading();
                        loadDataPallet(true);
                        loadData(true);
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }

                },
                error: function () {
                    common.notify('Can not load ajax function SavePalletProduct', 'error');
                    common.stopLoading();
                }
            });
        }
    }
    function SaveMapProduct() {
        
        if ($('#frmProduct').valid()) {
               
                $.ajax({
                    type: "POST",
                    url: "../ImportGoods/SaveMapProduct",
                    data: {
                        id: $('#hidIdM').val(),
                        codeProduct: $('#ddlCode').val(),
                        so_luong: $('#txtNumber').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                           
                            resetFormMainProduct();
                            loadDataPallet(true);
                            loadData(true);
                            dropdown.GetProductByGoods($('#ddlCodeProduct'), $('#hidIdM').val(), '');
                            dropdown.GetPallet($('#ddlCodePallet'), '');
                           
                           // loadData(true);
                        }
                        else {
                            if (response.Message == 'CHECK') {
                                common.popupOk('Cảnh báo', 'Mã sản phẩm trên đã được chọn cho xe. Bạn vui lòng chọn mã sản phẩm khác', 'error');

                            }
                            else {
                                common.notify(response.Message, 'error');
                            }
                                
                        }
                        common.stopLoading();
                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveMapProduct', 'error');
                        common.stopLoading();
                    }
                });
        }
    }

    function UpdateMapProduct() {

        if ($('#frmProduct').valid()) {

            $.ajax({
                type: "POST",
                url: "../ImportGoods/UpdateMapProduct",
                data: {
                    id: $('#hidIdM').val(),
                    codeProductCu: $('#hidIdproductCu').val(),
                    codeProduct: $('#ddlCode').val(),
                    so_luong: $('#txtNumber').val(),
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Cập nhật thành công', 'success');


                        resetFormMainProduct();
                        $('#hidIdproductCu').val('');
                        loadDataPallet(true);
                        loadData(true);
                        dropdown.GetProductByGoods($('#ddlCodeProduct'), $('#hidIdM').val(), '');
                       // dropdown.GetPallet($('#ddlCodePallet'), '');
                        // loadData(true);
                    }
                    else {
                        if (response.Message == 'CHECK') {
                            common.popupOk('Cảnh báo', 'Mã sản phẩm trên đã được chọn cho xe. Bạn vui lòng chọn mã sản phẩm khác', 'error');

                        }
                        else {
                            common.notify(response.Message, 'error');
                        }

                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateMapProduct', 'error');
                    common.stopLoading();
                }
            });
        }
    }
    function GetMapProducttById(Id, codeProducct) {
                $.ajax({
                    type: "GET",
                    url: "../ImportGoods/GetMapProducttById",
                    data: { id: Id, codeProducct: codeProducct},
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            $('#ddlCode').val(data.MPG_PRODUCT);
                            $('#txtNumber').val(data.MPG_NUMBER);
                            $('#txtNumberTT').val(data.MPG_REALITY);
                            $('#hidIdproductCu').val(data.MPG_PRODUCT);
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetMapProducttById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function GetMapPalletById(Id) {
        $.ajax({
            type: "GET",
            url: "../ImportGoods/GetMapPalletById",
            data: { id: Id},
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    var data = response.Data.Items[0];
                    $('#ddlCodePallet').val(data.MPP_PALLET);  
                    //dropdown.GetProductByGoods($('#ddlCodeProduct'), $('#hidIdM').val(), data.MPP_NUMBER);
                    $('#ddlCodeProduct').val(data.MPP_PRODUCT);
                    $('#txtNumberPallet').val(data.MPP_NUMBER); 
                    $('#hidIdpalletCu').val(data.MPP_ID);
                    $('#hidSoLuongCu').val(data.MPP_NUMBER);
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetMapPalletById', 'error');
                common.stopLoading();
            }
        });
        //}
    }

    function UpdateImportGoods() {
        if ($('#frmGoods').valid()) {
                $.ajax({
                    type: "POST",
                    url: "../ImportGoods/UpdateImportGoods",
                    data: {
                        idGoods: $('#hidIdM').val(),
                        licensePlates: $('#txtLicensePlates').val(),
                        licenseName: $('#txtLicenseName').val(),
                        fromDate: $('#txtFromDate').val(),
                        des: $('#txtDes').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            common.notify('Cập nhật thành công', 'success');


                            common.stopLoading();
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }

                    },
                    error: function () {
                        common.notify('Can not load ajax function UpdateImportGoods', 'error');
                        common.stopLoading();
                    }
                });
        }
    }

    function DeletePallet() {
        $.ajax({
            type: "POST",
            url: "../ImportGoods/DeletePallet",
            data: { Id: checkxoapallet },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');
                   
                    loadDataPallet(true);
                    loadData(true);
                    dropdown.GetPallet($('#ddlCodePallet'), '');
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function DeletePallet', 'error');
                common.stopLoading();
            }
        });
    }

    function DeleteProduct() {
        $.ajax({
            type: "POST",
            url: "../ImportGoods/DeleteProduct",
            data: { Id: checkxoa, IdProduct: checkxoaproduct },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');
                   
                    loadDataPallet(true);
                    loadData(true);
                    dropdown.GetProductByGoods($('#ddlCodeProduct'), $('#hidIdM').val(), '');
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function DeletePallet', 'error');
                common.stopLoading();
            }
        });
    }

   
}