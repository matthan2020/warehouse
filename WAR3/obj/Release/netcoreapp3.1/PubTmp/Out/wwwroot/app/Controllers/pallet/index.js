﻿var palletController = function () {
    var checkxoa = '';
    var checkxoastatus = 0;
    this.initialize = function () {

       
        loadData();
        registerEvents();
        registerControls();
         $('.dateTime').datepicker({
            //autoclose: true,
            format: 'dd/MM/yyyy'
        });
        
    }

    var registerEvents = function () {
       
        
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
      

       
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $("#btnCreate").on('click', function () {
            resetFormMaintainance();
            $('#hiddenNumber').prop('hidden', false);
            $('#bcTarget1').prop('hidden', true);
            $('#frmPallet').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDes: {
                        required: false
                    },
                }
            });
            //initTreeDropDownCategory();
            $('#modal-pallet-add-edit').modal('show');

        });
        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
            if (check == '0') {
                SavePallet();
            }
            else {
                UpdatePallet();
            }

        });
        $("#btnSearchNC").on('click', function () {
            loadData();

        });

      

        $('body').on('click', '.btn-edit', function (e) {

            $('#hidIdM').val(1);
            $('#hiddenNumber').prop('hidden', true);
            $('#bcTarget1').prop('hidden', false);
            var that = $(this).data('id');
            $('#frmPallet').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDes: {
                        required: false
                    },
                }
            });
            e.preventDefault();
            GetPalletById(that);
        });

        $('body').on('click', '.btn-detail', function (e) {
            
            var that = $(this).data('id');
            e.preventDefault();
            GetPalletDetailById(that);
        });

        $("#btnDetail").on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xem!", "error")
            }
            else {
                LoadListDetail();
            }
            

        });
      
       
       
       
       

        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!","error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeletePallet();
                });
            }

        });

       
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ",";
                    var chuoistatus = $(this).data('status')
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                        if (chuoistatus != '0') {
                            
                            checkxoastatus = checkxoastatus+1;
                        }
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                    checkxoastatus = 0;
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                var chuoistatus = $(this).data('status');
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                    if (chuoistatus != '0') {
                        checkxoastatus = checkxoastatus+1;
                    }
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
                if (chuoistatus == '0') {
                    checkxoastatus = checkxoastatus-1;
                }
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
        });

        //$('#btnImport').on('click', function () {
        //    $('#modal-import-product-excel').modal('show');
        //});

        $('#btnExport').on('click', function () {
           
            ExportExcel();
          
        });
        
        $('#btnPrint').on('click', function () {
            //$("#barcode").JsBarcode("Hi!");
            var printContents = document.getElementById('bcTarget').innerHTML;
            console.log(printContents);
            //var originalContents = document.body.innerHTML;

            //document.body.innerHTML = printContents;

           // window.print();

            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write(printContents);
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            //mywindow.document.write('</head><body >');
            //mywindow.document.write(data);
            //mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

        });
    
       
    }
    var registerControls = function () {
       // dropdown.GetProductType($('#ddlProductType'), '');
    }
    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        $('#txtNumber').val('');
        // initTreeDropDownCategory('');
        
        $('#txtDes').val('');
       

    }
 
    function loadData(isPageChanged) {
        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-pallet').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                status: $('#ddlStatus').val(),
                fromDate: $('#txtFromDate').val(),
                toDate: $('#txtToDate').val(),
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Pallet/GetAllPallet',
            dataType: 'json',
            success: function (response) {
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: common.configs.pageSize-10+i + 1,
                            CODE: item.CODE,
                            STATUS: item.STATUS == '0' ? 'Chưa sử dụng' : item.STATUS == '3' ? 'Đã xóa':'Đã sử dụng',
                            CREATE_DATE: item.CREATE_DATE,
                            CREATE_USER: item.CREATE_USER,
                            CHECK_STATUS: item.STATUS
                        });
                      
                        if (render != '') {
                            $('#tbl-pallet').html(render);
                        }
                        common.wrapPaging(response.Data.TotalRow, function () {
                            loadData(false);
                        }, isPageChanged);
                    });
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="7">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-pallet').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllImportGoods', 'error');
            }
        });
    }

    function SavePallet() {
        if ($('#frmPallet').valid()) {

            $.ajax({
                type: "POST",
                url: "../Pallet/SavePallet",
                data: {
                    number: $('#txtNumber').val(),
                    des: $('#txtDes').val()
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Thêm mới thành công', 'success');

                        $('#modal-pallet-add-edit').modal('hide');
                        resetFormMaintainance();

                        common.stopLoading();
                        loadData(true);
                    }
                    else {
                        if (response.Message == 'CHECK') {
                            common.notify('Code đã trùng. Vui lòng nhập Code khác', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                    }

                },
                error: function () {
                    common.notify('Can not load ajax function SaveProduct', 'error');
                    common.stopLoading();
                }
            });
        }
    }

    function GetPalletById(Id) {
        $.ajax({
            type: "GET",
            url: "../Pallet/GetPalletById",
            data: { id: Id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    var data = response.Data.Items[0];
                    $('#hidCode').val(data.CODE);
                    $('#txtDes').val(data.DESCRIPTION);
                    JsBarcode("#barcode", data.CODE);
                    $('#modal-pallet-add-edit').modal('show');
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetProductById', 'error');
                common.stopLoading();
            }
        });
        //}
    }

    function GetPalletDetailById(Id) {
        $('#bcTarget').html('');
        $.ajax({
            type: "GET",
            url: "../Pallet/GetPalletById",
            data: { id: Id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    var data = response.Data.Items[0];
                    var read = '<svg id="detailPallet"></svg>';
                    $('#bcTarget').append(read);
                    //JsBarcode("#detailPallet-" + i, idCode[i]);
                    JsBarcode("#detailPallet", data.CODE);
                    if (data.STATUS == '0') {
                        $('#print').prop('hidden', false);
                    }
                    else {
                        $('#print').prop('hidden', true);
                    }
                    $('#modal-pallet-detail').modal('show');
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetProductById', 'error');
                common.stopLoading();
            }
        });
        //}
    }

    function UpdatePallet() {
        if ($('#frmPallet').valid()) {

            $.ajax({
                type: "POST",
                url: "../Pallet/UpdatePallet",
                data: {
                    code: $('#hidCode').val(),
                    des: $('#txtDes').val()
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Cập nhật thành công', 'success');

                        $('#modal-pallet-add-edit').modal('hide');
                        resetFormMaintainance();
                        
                        common.stopLoading();
                        loadData(true);
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }

                },
                error: function () {
                    common.notify('Can not load ajax function UpdateProduct', 'error');
                    common.stopLoading();
                }
            });
        }
    }

    function LoadListDetail() {
       // document.getElementById(".checkPallet").remove();
        //$('#detailPallet').prop('hidden', true);
        $('#bcTarget').html('');
        var idCode = checkxoa.split(',');
        for (var i = 0; i <= idCode.length-1; i++) {
            if (i == 0 || i == idCode.length - 1) {

            }
            else {
                //JsBarcode(".detailPallet", data.CODE);
                //$('#bcTarget').append(JsBarcode("#detailPallet", idCode[i]));
                var read = '<svg id="detailPallet-' + i + '"></svg>';
                $('#bcTarget').append(read);
                JsBarcode("#detailPallet-"+i, idCode[i]);
                

                //var read = '<tr>' +
                //    '<th scope = "row" style = "text-align: center;" >' +
                //    '<input type="checkbox" class="ckcheck1">' +
                //    '</th>' +
                //    '<td><input type="text" class="productName" /></td>' +
                //    '<td><input type="number" name="fname" /></td>' +
                //    '</tr >';
               
            }
        }
        
        if (checkxoastatus == 0) {
            $('#print').prop('hidden', false);
        }
        else {
            $('#print').prop('hidden', true);
        }
        $('#modal-pallet-detail').modal('show');
    }

   

    function DeletePallet() {
        $.ajax({
            type: "POST",
            url: "../Pallet/DeletePallet",
            data: { Id: checkxoa },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');
                   
                    loadData(true);
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function DeleteContact', 'error');
                common.stopLoading();
            }
        });
    }

    function ExportExcel() {
        $.ajax({
            url: '../Pallet/ExportExcel',
            type: 'GET',
            data: {
                status: $('#ddlStatus').val(),
                fromDate: $('#txtFromDate').val(),
                toDate: $('#txtToDate').val(),
                keyword: $('#txtKeyword').val(),
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
   
}