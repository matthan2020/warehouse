﻿var userController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        GetUserByUserName();
        registerEvents();
        
    }

    var registerEvents = function () {
        $('#frmAccount').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                txtName: {
                    required: true
                },
            }
        });
        $('#frmChangePass').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                txtPass: {
                    required: true
                },
            }
        });
        $('#btnUpdate').on('click', function () {
            $('#txtName').prop('disabled',false);
            $('#txtCompany').prop('disabled', false);
            $('#txtAddress').prop('disabled', false);
            $('#txtEmail').prop('disabled', false);
            $('#txtPhone').prop('disabled', false);
            $('#btnSave').prop('hidden', false);
            $('#change_pass').prop('hidden', true);
        });
     

     
        $('#btnSave').on('click', function () {
           
                SaveUpdateUser();
           

        });

        $('#btnChangePass').on('click', function () {

            $('#btnSave').prop('hidden', true);
            $('#change_pass').prop('hidden', false);


        });

        $('#btnSavePass').on('click', function () {

            SaveUpdatePass();


        });
       
        $('#btnCancel').on('click', function () {

            $('#txtPass').val('');
            $('#txtPassNew').val('');
            $('#txtPassNewRetype').val('');
            $('#change_pass').prop('hidden', true);


        });
     
    }
   

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        $('#txtID').val('');
       // initTreeDropDownCategory('');

        $('#txtMaSP').val('');
        $('#txtTenSP').val('');
        $('#txtID').prop('disabled', false);
      

    }

    function SaveUpdateUser() {
        alert($('#txtCompany').val());
        if ($('#frmAccount').valid()) {
            if ( $('#txtPhone').val().length > 9 && $('#txtPhone').val().length < 12) {

                $.ajax({
                    type: "POST",
                    url: "/User/SaveUpdateUser",
                    data: {
                        //NAME: $('#txtName').val(),
                        //NAMECOMPANY: $('#txtCompany').val(),
                        //ADDRESS: $('#txtAddress').val(),
                        //EMAIL: $('#txtEmail').val(),
                        //PHONE: $('#txtPhone').val(),
                        Name: $('#txtName').val(),
                        Company: $('#txtCompany').val(),
                        Address: $('#txtAddress').val(),
                        Email: $('#txtEmail').val(),
                        Phone: $('#txtPhone').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.Success == true) {
                            common.notify('Sửa thành công', 'success');

                            resetFormMaintainance();

                            $('#txtName').prop('disabled', true);
                            $('#txtCompany').prop('disabled', true);
                            $('#txtAddress').prop('disabled', true);
                            $('#txtEmail').prop('disabled', true);
                            $('#txtPhone').prop('disabled', true);
                            $('#btnSave').prop('hidden', true);
                            $('#change_pass').prop('hidden', true);
                        }
                        else {
                            
                                common.notify(response.Message, 'error');
                        }
                        common.stopLoading();
                    },
                    error: function () {
                        common.notify('Has an error in save product progress', 'error');
                        common.stopLoading();
                    }
                });
            }
            else {
                common.popupOk('Cảnh báo', 'Số điện thoại phải là 10 số hoặc 11 số.Mời bạn nhập lại', 'error');
            }
        }
    }

    function GetUserByUserName() {
                $.ajax({
                    type: "GET",
                    url: "/User/GetUserByUserName",
                    
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data;
                            $('#txtUserName').val(data.USER_NAME);
                            $('#txtName').val(data.NAME);
                            $('#txtCompany').val(data.NAME_COMPANY);
                            $('#txtAddress').val(data.ADDRESS);
                            $('#txtEmail').val(data.EMAIL);
                            $('#txtPhone').val(data.PHONE); 
                            $('#hidIdM').val(data.PASSWORD);
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function (status) {
                        common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function SaveUpdatePass() {
        if ($('#frmChangePass').valid()) {
            if($('#hidIdM').val() == $('#txtPass').val()){
                if (($('#txtPassNew').val() == $('#txtPassNewRetype').val())) {
                    //var passnew=
                    $.ajax({
                        type: "POST",
                        url: "/User/SaveUpdatePass",
                        data: {
                            PassNew: $('#txtPassNew').val(),
                        },
                        dataType: "json",
                        beforeSend: function () {
                            common.startLoading();
                        },
                        success: function (response) {
                            if (response.Success == true) {
                                common.notify('Đổi mật khẩu thành công', 'success');

                                //resetFormMaintainance();
                                $('#hidIdM').val($('#txtPassNew').val());
                                $('#txtPass').val('');
                                $('#txtPassNew').val('');
                                $('#txtPassNewRetype').val('');
                                $('#change_pass').prop('hidden', true);
                               
                            }
                            else {

                                common.notify(response.Message, 'error');
                            }
                            common.stopLoading();
                        },
                        error: function () {
                            common.notify('Has an error in save product progress', 'error');
                            common.stopLoading();
                        }
                    });
                }
                else {
                    common.popupOk('Cảnh báo', 'Mật khẩu mới và nhập lại mật khẩu mới không giống nhau. Mời bạn nhập lại mật khẩu mới', 'error');
                }
            }
            else {
                common.popupOk('Cảnh báo', 'Mật khẩu cũ không đúng. Mời bạn nhập lại mật khẩu cũ', 'error');
        }
               
            
        }
    }
    

   
   
}