﻿var dropdown = {
    
    GetProductType: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../ProductType/GetProductType",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.PT_ID == data) {
                        render += "<option value='" + item.PT_ID + "' selected>" + item.PT_ID + ' - ' + item.PT_NAME + "</option>";
                    }
                    else {
                        render += "<option value='" + item.PT_ID + "'>" + item.PT_ID + ' - ' + item.PT_NAME + "</option>";
                    }
                    
                });
                ddltype.html(render);
            }
        });
    },
    GetProduct: function (ddltype, data) {
        return $.ajax({
            type: "POST",
            url: "../Product/GetAllProduct",
            data: { keyword:'',},
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.WR_PR_CODE == data) {
                        render += "<option value='" + item.WR_PR_CODE + "' selected>" + item.WR_PR_CODE + "</option>";
                    }
                    else {
                        render += "<option value='" + item.WR_PR_CODE + "'>" + item.WR_PR_CODE +  "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetProductByGoods: function (ddltype,idGoods, data) {
        return $.ajax({
            type: "GET",
            url: "../ImportGoods/GetProductByGoods",
            data: { idGoods: idGoods, },
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.MPG_PRODUCT == data) {
                        render += "<option value='" + item.MPG_PRODUCT + "' selected>" + item.MPG_PRODUCT + "</option>";
                    }
                    else {
                        render += "<option value='" + item.MPG_PRODUCT + "'>" + item.MPG_PRODUCT + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetPallet: function (ddltype,  data) {
        return $.ajax({
            type: "GET",
            url: "../ImportGoods/GetPallet",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.CODE == data) {
                        render += "<option value='" + item.CODE + "' selected>" + item.CODE + "</option>";
                    }
                    else {
                        render += "<option value='" + item.CODE + "'>" + item.CODE + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetLocation: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Location/GetAllLocationNoPage",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.LC_CODE == data) {
                        render += "<option value='" + item.LC_CODE + "' selected>" + item.LC_CODE + ' - ' + item.LC_NAME + "</option>";
                    }
                    else {
                        render += "<option value='" + item.LC_CODE + "'>" + item.LC_CODE + ' - ' + item.LC_NAME + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
}