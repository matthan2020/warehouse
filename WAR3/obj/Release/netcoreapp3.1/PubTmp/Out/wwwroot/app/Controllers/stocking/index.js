﻿var stockingController = function () {
    var checkxoa = '';
    var checkxoastatus = 0;
    this.initialize = function () {

       
        loadData();
        registerEvents();
        registerControls();
        $('.dateTime').datepicker({
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            //format: 'dd/MM/yyyy',
            todayHighlight: true
        }); 
        
    }

    var registerEvents = function () {
       
        
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
      

       
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

       
       

      

        $('body').on('click', '.btn-edit', function (e) {

            var that = $(this).data('id');
            e.preventDefault();
            $('#frmStocking').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    ddlLocation: {
                        required: true
                    },
                }
            });
            GetStockingById(that);
        });

        $('#btnSave').on('click', function () {

            UpdateStocking();
        });

     
      
       
       
       
       

        

       
       
      

      
        
        
    
       
    }
    var registerControls = function () {
        dropdown.GetLocation($('#ddlLocation'), '');
    }
 
    function loadData(isPageChanged) {
        
        //$.ajax({
        //    type: 'GET',
        //    url: 'http://192.168.1.57:8081/api/Contact/GetAllContactPage?pageIndex=1&pageSize=10',
        //    dataType: 'json',
        //    success: function (response) {
        //        console.log(response);
        //    },
        //    error: function () {
        //        common.notify('Can not load ajax function GetAllStocking', 'error');
        //    }
        //});

        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-stocking').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                fromDate: $('#txtFromDate').val(),
                toDate: $('#txtToDate').val(),
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Stocking/GetAllStocking',
            dataType: 'json',
            success: function (response) {
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: common.configs.pageSize-10+i + 1,
                            MPP_PALLET: item.MPP_PALLET,
                            MPP_PRODUCT: item.MPP_PRODUCT,
                            MPP_NUMBER: item.MPP_NUMBER,
                            MPP_LOCATION: item.MPP_LOCATION,
                            MPP_CREATEDATE: item.MPP_CREATEDATE,
                            MPP_CREATEUSER: item.MPP_CREATEUSER,
                            MPP_ID: item.MPP_ID
                        });
                      
                       
                    });
                    if (render != '') {
                        $('#tbl-stocking').html(render);
                    }
                    common.wrapPaging(response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="8">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-stocking').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllStocking', 'error');
            }
        });
    }

   

    function GetStockingById(Id) {
        $.ajax({
            type: "GET",
            url: "../Stocking/GetStockingById",
            data: { id: Id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    var data = response.Data.Items[0];
                    $('#hidID').val(data.MPP_ID);
                    $('#txtPallet').val(data.MPP_PALLET);
                    $('#txtProduct').val(data.MPP_PRODUCT);
                    $('#txtNumber').val(data.MPP_NUMBER);
                    $('#ddlLocation').val(data.MPP_LOCATION);
                    $('#modal-stocking-add-edit').modal('show');
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetStockingById', 'error');
                common.stopLoading();
            }
        });
        //}
    }

    
    function UpdateStocking() {
        if ($('#frmStocking').valid()) {

            $.ajax({
                type: "POST",
                url: "../Stocking/UpdateStocking",
                data: {
                    id: $('#hidID').val(),
                    location: $('#ddlLocation').val()
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Cập nhật thành công', 'success');

                        $('#modal-stocking-add-edit').modal('hide');
                        //resetFormMaintainance();
                        
                       
                        loadData(true);
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateProduct', 'error');
                    common.stopLoading();
                }
            });
        }
    }

    
   
}