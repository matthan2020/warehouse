﻿var productController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        loadData();
        registerEvents();
        registerControls();
        
    }

    var registerEvents = function () {
        //$('#frmProduct').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        ddlProductType: {
        //            required: true
        //        },
        //        txtCode: {
        //            required: true
        //        },
        //        txtModel: {
        //            required: false
        //        },
        //        txtName: {
        //            required: false
        //        },
        //        txtNameVN: {
        //            required: false
        //        },
        //        txtContact: {
        //            required: false
        //        },
        //        txtDes: {
        //            required: false
        //        },
        //    }
        //});
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $("#btnCreate").on('click', function () {
            resetFormMaintainance();
            //initTreeDropDownCategory();
            $('#modal-product-add-edit').modal('show');

        });

        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
            if (check == '0') {
                SaveProduct();
            }
            else {
                UpdateProduct();
            }

        });

        $('body').on('click', '.btn-edit', function (e) {

            $('#hidIdM').val(1);
            $('#txtCode').prop('disabled', true);
            var that = $(this).data('id');
            e.preventDefault();
            GetProductById(that);
        });

        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!","error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteProduct();
                });
            }

        });
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });

        $('#btnImport').on('click', function () {
            $('#modal-import-product-excel').modal('show');
        });

        $('#btnExport').on('click', function () {
            ExportExcel();
          
        });

        $('#btnImportExcel').on('click', function () {
            ImportExcel();
            
        });
    }
    var registerControls = function () {
        dropdown.GetProductType($('#ddlProductType'), '');
    }

    function ImportExcel() {
        var fileUpload = $("#fileInputExcel").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();
        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {
            fileData.append("files", files[i]);
        }
        //console.log(fileData);
       
        // Adding one more key to FormData object  
         //fileData.append('categoryId', $('#ddlCategoryIdImportExcel').combotree('getValue'));
       

       
        $.ajax({
            url: '../Product/ImportExcel',
            type: 'POST',
            data: fileData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                console.log(response);
                if (response.Success == true) {
                    if (response.Message == 'SUCCESS') {
                        $('#modal-import-product-excel').modal('hide');
                        loadData();
                        common.notify('Thêm mới thành công' + response.Data + ' bản ghi', 'success');
                    }
                    else {
                        $('#modal-import-product-excel').modal('hide');
                        loadData();
                        common.notify(response.Message, 'success');
                        window.location.href = response.Data;
                    }





                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();

            },
            error: function () {
                common.notify("Can not load ajax function ImportExcel", 'error');
                common.stopLoading();
            }
        });
    }
    function loadData(isPageChanged) {
        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-product').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Product/GetAllProduct',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.Data.Success == true) {
                    if (response.Data.TotalRow > 0) {
                        $.each(response.Data.Items, function (i, item) {
                            render += Mustache.render(template, {
                                STT: i + 1,
                                WR_PR_CODE: item.WR_PR_CODE,
                                WR_PR_TYPE: item.WR_PR_TYPE,
                                WR_PR_MODEL: item.WR_PR_MODEL,
                                WR_PR_NAME: item.WR_PR_NAME,
                                WR_PR_NAME_VN: item.WR_PR_NAME_VN,
                                WR_PR_CONTACT: item.WR_PR_CONTACT,
                                WR_PR_DESCRIPTION: item.WR_PR_DESCRIPTION,
                                WR_PR_CREATEDATE: item.WR_PR_CREATEDATE,
                                WR_PR_CREATEUSER: item.WR_PR_CREATEUSER,
                            });

                           
                        });
                        if (render != '') {
                            $('#tbl-product').html(render);
                        }
                        common.wrapPaging(response.Data.TotalRow, function () {
                            loadData(false);
                        }, isPageChanged);
                    }
                    else {
                        render = '<tr>' +
                            '    <td colspan="12">Không có dữ liệu</td>' +
                            '</tr>';
                        $('#tbl-product').html(render);
                    }
                    $('#lblTotalRecords').text(response.Data.TotalRow);
                }
                else {
                    common.notify(response.Data.Message, 'error');
                }
               
            },
            error: function () {
                common.notify('Can not load ajax function GetAllProduct', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        $('#ddlProductType').val('');
       // initTreeDropDownCategory('');

        $('#txtCode').val('');
        $('#txtModel').val('');
        $('#txtCode').prop('disabled', false);
        
        $('#txtName').val('');
        $('#txtNameVN').val('');
        $('#txtContact').val('');
        $('#txtDes').val('');

    }

    function SaveProduct() {
        if ($('#frmProduct').valid()) {

                $.ajax({
                    type: "POST",
                    url: "../Product/SaveProduct",
                    data: {
                        productType: $('#ddlProductType').val(),
                        code: $('#txtCode').val(),
                        model: $('#txtModel').val(),
                        name: $('#txtName').val(),
                        nameVN: $('#txtNameVN').val(),
                        contact: $('#txtContact').val(),
                        des: $('#txtDes').val()
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            $('#modal-product-add-edit').modal('hide');
                            resetFormMaintainance();

                            common.stopLoading();
                            loadData(true);
                        }
                        else {
                            if (response.Message == 'CHECK') {
                                common.notify('Code đã trùng. Vui lòng nhập Code khác', 'error');
                            }
                            else {
                                common.notify(response.Message, 'error');
                            }
                        }
                     
                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveProduct', 'error');
                        common.stopLoading();
                    }
                });
        }
    }

    function GetProductById(Id) {
                $.ajax({
                    type: "GET",
                    url: "../Product/GetProductById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            $('#txtCode').val(data.WR_PR_CODE);
                            $('#txtModel').val(data.WR_PR_MODEL);
                            $('#txtName').val(data.WR_PR_NAME);
                            $('#txtNameVN').val(data.WR_PR_NAME_VN);
                            $('#txtContact').val(data.WR_PR_CONTACT);
                            $('#txtDes').val(data.WR_PR_DESCRIPTION);
                            $('#ddlProductType').val(data.WR_PR_TYPE);
                            $('#modal-product-add-edit').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetProductById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function UpdateProduct() {
        if ($('#frmProduct').valid()) {

        $.ajax({
            type: "POST",
            url: "../Product/UpdateProduct",
            data: {
                productType: $('#ddlProductType').val(),
                code: $('#txtCode').val(),
                model: $('#txtModel').val(),
                name: $('#txtName').val(),
                nameVN: $('#txtNameVN').val(),
                contact: $('#txtContact').val(),
                des: $('#txtDes').val()
            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Cập nhật thành công', 'success');

                    $('#modal-product-add-edit').modal('hide');
                    resetFormMaintainance();

                    common.stopLoading();
                    loadData(true);
                }
                else {
                        common.notify(response.Message, 'error');
                }
               
            },
            error: function () {
                common.notify('Can not load ajax function UpdateProduct', 'error');
                common.stopLoading();
            }
        });
        }
    }

    function DeleteProduct() {
        $.ajax({
            type: "POST",
            url: "../Product/DeleteProduct",
            data: { Id: checkxoa },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                common.notify('Delete successful', 'success');
                common.stopLoading();
                loadData(true);
            },
            error: function () {
                common.notify('Can not load ajax function DeleteProduct', 'error');
                common.stopLoading();
            }
        });
    }

    function ExportExcel() {
        $.ajax({
            url: '../Product/ExportExcel',
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
   
}