using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.Implementation.Repositories;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Identity;
using WAR3_DAL.Entities;
using Microsoft.AspNetCore.Http;

namespace WAR3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddControllersWithViews();
          

            services.AddTransient<IUserRepositories, UserRepositories>();

            //services.AddScoped
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddScoped<IUserClaimsPrincipalFactory<UserViewModel>, CustomClaimsPrincipalFactory>();
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddTransient<IProductTypeRepositories, ProductTypeRepositories>();
            services.AddTransient<IProductRepositories, ProductRepositories>();
            services.AddTransient<ILocationRepositories, LocationRepositories>();
            services.AddTransient<IContactRepositories, ContactRepositories>();
            services.AddTransient<IImportGoodsRepositories, ImportGoodsRepositories>();
            services.AddTransient<IPalletRepositories, PalletRepositories>();
            services.AddTransient<IStockingRepositories, StockingRepositories>();
            //services.Configure<IdentityOptions>(options =>
            //{
            //    // Password settings
            //    options.Password.RequireDigit = true;
            //    options.Password.RequiredLength = 6;
            //    options.Password.RequireNonAlphanumeric = false;
            //    options.Password.RequireUppercase = false;
            //    options.Password.RequireLowercase = false;

            //    // Lockout settings
            //    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
            //    options.Lockout.MaxFailedAccessAttempts = 10;

            //    // User settings
            //    options.User.RequireUniqueEmail = true;
            //});
            services.AddSession();
            //services.AddScoped<Microsoft.AspNetCore.Identity.IUserClaimsPrincipalFactory<User>, CustomClaimsPrincipalFactory>();
            //services.AddScoped<UserClaimsPrincipalFactory<User>,CustomClaimsPrincipalFactory>();
            //services.AddScoped<IUserClaimsPrincipalFactory<User>, CustomClaimsPrincipalFactory>();
            //services.AddScoped<UserManager<User>, UserManager<User>>();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // Use the default property (Pascal) casing
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();

                // Configure a custom converter
            });
            IMvcBuilder builder = services.AddRazorPages();
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

//#if DEBUG
            if (environment == Environments.Development)
            {
                builder.AddRazorRuntimeCompilation();
            }
//#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            //app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Login}/{id?}");
            });
        }
    }
}
