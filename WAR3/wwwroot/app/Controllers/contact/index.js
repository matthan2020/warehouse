﻿var contactController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //$('#txtCreateDate').datepicker({
        //    autoclose: true,
        //    format: 'dd/mm/yyyy'
        //});;
       
        //loadData();
        $('#txtCreateDate').datepicker({
            autoclose: true,
            //dateFormat: 'dd/mm/yy',
            format: 'dd/mm/yyyy',
        });
        $('#txtReceiverDate').datepicker({
            //autoclose: true,
            autoclose: true,
            format: 'dd/mm/yyyy',
            //format: 'dd/MM/yyyy',
        });

        //$('#txtReceiver').datepicker({
        //    //autoclose: true,
        //    //autoclose: true,
        //    //timeFormat: "hh:mm:ss"
        //    format: 'HH:mm:SS',
        //    //format: 'dd/MM/yyyy',
        //});
        
        
        loadData();
        registerEvents();
        registerControls();
      

        
    }

    var registerEvents = function () {
       
        
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $("#btnCreate").on('click', function () {
            resetFormMaintainance();
            $('#frmContact').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtCampany: {
                        required: false
                    },
                    txtPhone: {
                        required: false
                    },
                    txtEmail: {
                        required: false
                    },
                    txtReceiverUser: {
                        required: false
                    },
                    txtDes: {
                        required: false
                    },
                }
            });
            $('#txtCreateDate').val(common.dateLoading());
            $('#txtReceiverDate').val(common.dateLoading());
            $('#txtReceiver').val(common.giophutgiayLoading());
            //initTreeDropDownCategory();
            $('#modal-contact-add-edit').modal('show');

        });

        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
            if (check == '0') {
                SaveContact();
            }
            else {
                UpdateContact();
            }

        });

        $('body').on('click', '.btn-edit', function (e) {

            $('#hidIdM').val(1);
            $('#txtCode').prop('disabled', true);
            var that = $(this).data('id');
            $('#frmContact').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtCampany: {
                        required: false
                    },
                    txtPhone: {
                        required: false
                    },
                    txtEmail: {
                        required: false
                    },
                    txtReceiverUser: {
                        required: false
                    },
                    txtDes: {
                        required: false
                    },
                }
            });
            e.preventDefault();
            GetContactById(that);
        });

        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!","error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteContact();
                });
            }

        });
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });

        //$('#btnImport').on('click', function () {
        //    $('#modal-import-product-excel').modal('show');
        //});

        $('#btnExport').on('click', function () {
            ExportExcel();
          
        });

        //$('#btnImportExcel').on('click', function () {
        //    var fileUpload = $("#fileInputExcel").get(0);
        //    var files = fileUpload.files;

        //    // Create FormData object  
        //    var fileData = new FormData();
        //    // Looping over all files and add it to FormData object  
        //    for (var i = 0; i < files.length; i++) {
        //        fileData.append("files", files[i]);
        //    }
        //    // Adding one more key to FormData object  
        //   // fileData.append('categoryId', $('#ddlCategoryIdImportExcel').combotree('getValue'));
        //    $.ajax({
        //        url: '/Product/ImportExcel',
        //        type: 'POST',
        //        data: fileData,
        //        processData: false,  // tell jQuery not to process the data
        //        contentType: false,  // tell jQuery not to set contentType
        //        beforeSend: function () {
        //            common.startLoading();
        //        },
        //        success: function (response) {
        //            console.log(response);
        //            if (response.success == true) {
        //                if (response.message == 'SUCCESS') {
        //                    $('#modal-import-product-excel').modal('hide');
        //                    loadData();
        //                    common.notify('Thêm mới thành công' + response.data + ' bản ghi', 'success');
        //                }
        //                else {
        //                    $('#modal-import-product-excel').modal('hide');
        //                    loadData();
        //                    common.notify(response.message, 'success');
        //                    window.location.href = response.data;
        //                }
                       

                      

                       
        //            }
        //            else {
        //                common.notify(response.message, 'error');
        //            }
        //            common.stopLoading();

        //        },
        //        error: function () {
        //            common.notify("Can not load ajax function ImportExcel", 'error');
        //            common.stopLoading();
        //        }
        //    });
        //});
    }
    var registerControls = function () {
       // dropdown.GetProductType($('#ddlProductType'), '');
    }
    function loadData(isPageChanged) {
        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-contact').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Contact/GetAllContact',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: i + 1,
                            CT_CODE: item.CT_CODE,
                            CT_TYPE: item.CT_TYPE=='NCC'?'Nhà cung cấp':'Khách hàng',
                            CT_NAME: item.CT_NAME,
                            CT_COMPANY: item.CT_COMPANY,
                            CT_ADDRESS: item.CT_ADDRESS,
                            CT_EMAIL: item.CT_EMAIL,
                            CT_PHONE: item.CT_PHONE,
                            CT_RECEIVER_USE: item.CT_RECEIVER_USER,
                            CT_DESCRIPTION: item.CT_DESCRIPTION,
                            CT_RECEIVER_DATE: item.CT_RECEIVER_DATE,
                            CT_CREATEDATE: item.CT_CREATEDATE,
                            CT_CREATEUSER: item.CT_CREATEUSER,
                        });
                      
                       
                    });
                    if (render != '') {
                        $('#tbl-contact').html(render);
                    }
                    common.wrapPaging(response.Data.TotalRow, function () {
                        //alert(1);
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="15">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-contact').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllContact', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        //$('#ddlProductType').val('');
       // initTreeDropDownCategory('');

        $('#txtCode').val('');
        $('#txtCampany').val('');
        $('#txtCode').prop('disabled', false);
        
        $('#txtName').val('');
        $('#txtAddress').val('');
        $('#txtEmail').val('');

        $('#txtPhone').val('');
        $('#txtReceiverUser').val('');
        //$('#txtReceiverDate').val('');

        //$('#txtReceiver').val('');

        //$('#txtCreateDate').val('');

        $('#txtDes').val('');

    }

    function SaveContact() {
        if ($('#frmContact').valid()) {
            if (($('#txtPhone').val().length > 9 && $('#txtPhone').val().length < 12) || ($('#txtPhone').val() == "" || $('#txtPhone').val() == null || $('#txtPhone').val() == undefined)) {
                $.ajax({
                    type: "POST",
                    url: "../Contact/SaveContact",
                    data: {
                        type: $('#ddlType').val(),
                        code: $('#txtCode').val(),
                        name: $('#txtName').val(),
                        company: $('#txtCampany').val(),
                        address: $('#txtAddress').val(),
                        email: $('#txtEmail').val(),
                        phone: $('#txtPhone').val(),
                        receiverUser: $('#txtReceiverUser').val(),
                        receiverDate: $('#txtReceiverDate').val() + ' ' + $('#txtReceiver').val(),
                        des: $('#txtDes').val(),
                        createDate: $('#txtCreateDate').val()
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            $('#modal-contact-add-edit').modal('hide');
                            resetFormMaintainance();

                            common.stopLoading();
                            loadData(true);
                        }
                        else {
                            if (response.Message == 'CHECK') {
                                common.notify('Code đã trùng. Vui lòng nhập Code khác', 'error');
                            }
                            else {
                                common.notify(response.Message, 'error');
                            }
                        }

                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveProduct', 'error');
                        common.stopLoading();
                    }
                });
            }
            else {
                common.popupOk('Cảnh báo','Số điện thoại phải là 10 số hoặc 11 số.Mời bạn nhập lại', 'error');
            }  
        }
    }

    function GetContactById(Id) {
                $.ajax({
                    type: "GET",
                    url: "../Contact/GetContactById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            console.log(data);
                            $('#txtCode').val(data.CT_CODE);
                            $('#txtName').val(data.CT_NAME);
                            $('#txtCampany').val(data.CT_COMPANY);
                            $('#txtAddress').val(data.CT_ADDRESS);
                            $('#txtEmail').val(data.CT_EMAIL);
                            $('#txtPhone').val(data.CT_PHONE == '0' ? null : data.CT_PHONE); 
                            $('#ddlType').val(data.CT_TYPE);
                            $('#txtReceiverUser').val(data.CT_RECEIVER_USER);
                            $('#txtReceiver').val(data.CT_RECEIVER_DATE.split(" ")[1]);
                            $('#txtReceiverDate').val(data.CT_RECEIVER_DATE.split(" ")[0]);
                            $('#txtDes').val(data.CT_DESCRIPTION);
                            $('#txtCreateDate').val(data.CT_CREATEDATE);
                            $('#modal-contact-add-edit').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetContactById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function UpdateContact() {
        if ($('#frmContact').valid()) {
            if (($('#txtPhone').val().length > 9 && $('#txtPhone').val().length < 12) || ($('#txtPhone').val() == "" || $('#txtPhone').val() == null || $('#txtPhone').val() == undefined)) {
                $.ajax({
                    type: "POST",
                    url: "../Contact/UpdateContact",
                    data: {
                        type: $('#ddlType').val(),
                        code: $('#txtCode').val(),
                        name: $('#txtName').val(),
                        company: $('#txtCampany').val(),
                        address: $('#txtAddress').val(),
                        email: $('#txtEmail').val(),
                        phone: $('#txtPhone').val(),
                        receiverUser: $('#txtReceiverUser').val(),
                        receiverDate: $('#txtReceiverDate').val() + ' ' + $('#txtReceiver').val(),
                        des: $('#txtDes').val(),
                        createDate: $('#txtCreateDate').val()
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            common.notify('Cập nhật thành công', 'success');

                            $('#modal-contact-add-edit').modal('hide');
                            resetFormMaintainance();

                            common.stopLoading();
                            loadData(true);
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }

                    },
                    error: function () {
                        common.notify('Can not load ajax function UpdateContact', 'error');
                        common.stopLoading();
                    }
                });
            }
            else {
                common.popupOk('Cảnh báo', 'Số điện thoại phải là 10 số hoặc 11 số.Mời bạn nhập lại', 'error');
            }
        }
    }

    function DeleteContact() {
        $.ajax({
            type: "POST",
            url: "../Contact/DeleteContact",
            data: { Id: checkxoa },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                common.notify('Xóa thành công', 'success');
                common.stopLoading();
                loadData(true);
            },
            error: function () {
                common.notify('Can not load ajax function DeleteContact', 'error');
                common.stopLoading();
            }
        });
    }

    function ExportExcel() {
        $.ajax({
            url: '../Contact/ExportExcel',
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
   
}