﻿var locationController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        loadData();
        registerEvents();
        registerControls();
        
    }

    var registerEvents = function () {
       
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $("#btnCreate").on('click', function () {
            resetFormMaintainance();
            $('#frmLocation').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtCode: {
                        required: true
                    },
                    txtName: {
                        required: false
                    },

                }
            });
            //initTreeDropDownCategory();
            $('#modal-location-add-edit').modal('show');

        });

        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
            if (check == '0') {
                SaveLocation();
            }
            else {
                UpdateLocation();
            }

        });

        $('body').on('click', '.btn-edit', function (e) {

            $('#hidIdM').val(1);
            $('#txtCode').prop('disabled', true);
            var that = $(this).data('id');
            $('#frmLocation').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtCode: {
                        required: true
                    },
                    txtName: {
                        required: false
                    },

                }
            });
            e.preventDefault();
            GetLocationById(that);
        });

        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!","error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteLocation();
                });
            }

        });
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });

        $('#btnImport').on('click', function () {
            $('#modal-import-location-excel').modal('show');
        });

        $('#btnExport').on('click', function () {
            ExportExcel();
          
        });

        $('#btnImportExcel').on('click', function () {
            var fileUpload = $("#fileInputExcel").get(0);
            var files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();
            // Looping over all files and add it to FormData object  
            for (var i = 0; i < files.length; i++) {
                fileData.append("files", files[i]);
            }
            // Adding one more key to FormData object  
           // fileData.append('categoryId', $('#ddlCategoryIdImportExcel').combotree('getValue'));
            $.ajax({
                url: '../Location/ImportExcel',
                type: 'POST',
                data: fileData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        if (response.Message == 'SUCCESS') {
                            $('#modal-import-location-excel').modal('hide');
                            loadData();
                            common.notify('Thêm mới thành công' + response.Data + ' bản ghi', 'success');
                        }
                        else {
                            $('#modal-import-location-excel').modal('hide');
                            loadData();
                            common.notify(response.Message, 'success');
                            window.location.href = response.Data;
                        }
                       

                      

                       
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }
                    common.stopLoading();

                },
                error: function () {
                    common.notify("Can not load ajax function ImportExcel", 'error');
                    common.stopLoading();
                }
            });
        });
    }
    var registerControls = function () {
        //dropdown.GetProductType($('#ddlProductType'), '');
    }
    function loadData(isPageChanged) {
        $('.ckcheck').prop('checked', false);
        var template = $('#table-template-location').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Location/GetAllLocation',
            dataType: 'json',
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                console.log(response);
                if (response.Success == true) {
                    if (response.Data.TotalRow > 0) {
                        $.each(response.Data.Items, function (i, item) {
                            render += Mustache.render(template, {
                                STT: i + 1,
                                LC_CODE: item.LC_CODE,
                                LC_NAME: item.LC_NAME,
                                LC_UPDATEUSER: item.LC_UPDATEUSER,
                                LC_UPDATEDATE: item.LC_UPDATEDATE,
                                LC_CREATEDATE: item.LC_CREATEDATE,
                                LC_CREATEUSER: item.LC_CREATEUSER,
                            });

                           
                        });
                        if (render != '') {
                            $('#tbl-content-location').html(render);
                        }
                        common.wrapPaging(response.Data.TotalRow, function () {
                            loadData(false);
                        }, isPageChanged);
                    }
                    else {
                        render = '<tr>' +
                            '    <td colspan="9">Không có dữ liệu</td>' +
                            '</tr>';
                        $('#tbl-content-location').html(render);
                    }
                    $('#lblTotalRecords').text(response.Data.TotalRow);
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function GetAllLocation', 'error');
                common.stopLoading();
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
       // initTreeDropDownCategory('');

        $('#txtCode').val('');
        $('#txtCode').prop('disabled', false);
        
        $('#txtName').val('');

    }

    function SaveLocation() {
        if ($('#frmLocation').valid()) {

                $.ajax({
                    type: "POST",
                    url: "../Location/SaveLocation",
                    data: {
                        code: $('#txtCode').val(),
                        name: $('#txtName').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            $('#modal-location-add-edit').modal('hide');
                            resetFormMaintainance();

                           
                            loadData(true);
                        }
                        else {
                            if (response.Message == 'CHECK') {
                                common.notify('Code đã trùng. Vui lòng nhập Code khác', 'error');
                            }
                            else {
                                common.notify(response.Message, 'error');
                            }
                        }
                        common.stopLoading();
                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveProduct', 'error');
                        common.stopLoading();
                    }
                });
        }
    }

    function GetLocationById(Id) {
                $.ajax({
                    type: "GET",
                    url: "../Location/GetLocationById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            $('#txtCode').val(data.LC_CODE);
                            $('#txtName').val(data.LC_NAME);
                           
                            $('#modal-location-add-edit').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetLocationById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function UpdateLocation() {
        if ($('#frmLocation').valid()) {

        $.ajax({
            type: "POST",
            url: "../Location/UpdateLocation",
            data: {
                code: $('#txtCode').val(),
                name: $('#txtName').val()
            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Cập nhật thành công', 'success');

                    $('#modal-location-add-edit').modal('hide');
                    resetFormMaintainance();

                  
                    loadData(true);
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateLocation', 'error');
                common.stopLoading();
            }
        });
        }
    }

    function DeleteLocation() {
        $.ajax({
            type: "POST",
            url: "../Location/DeleteLocation",
            data: { Id: checkxoa },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');
                   
                    loadData(true);
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function DeleteLocation', 'error');
                common.stopLoading();
            }
        });
    }

    function ExportExcel() {
        $.ajax({
            url: '../Location/ExportExcel',
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
   
}