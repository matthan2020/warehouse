﻿var productTypeController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        loadData();
        registerEvents();
        
    }

    var registerEvents = function () {
        //$('#frmMaintainance').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        txtID: {
        //            required: true
        //        },
        //        txtTenSP: {
        //            required: true
        //        }
        //    }
        //});
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $("#btnCreate").on('click', function () {
            resetFormMaintainance();
            //initTreeDropDownCategory();
            $('#frmMaintainance').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                txtID: {
                    required: true
                },
                txtTenSP: {
                    required: true
                }
            }
        });
            $('#modal-add-edit').modal('show');

        });

        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
            if (check == '0') {
                SaveProductType();
            }
            else {
                UpdateProductType();
            }

        });

        $('body').on('click', '.btn-edit', function (e) {

            $('#hidIdM').val(1);
            $('#txtID').prop('disabled', true);
            var that = $(this).data('id');
            $('#frmMaintainance').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtID: {
                        required: true
                    },
                    txtTenSP: {
                        required: true
                    }
                }
            });
            e.preventDefault();
            GetProductTypeById(that);
        });

        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!","error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteProductType();
                });
            }

        });
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            //console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            //console.log(checkxoa);
        });

        $('#btnImport').on('click', function () {
            $('#modal-import-excel').modal('show');
        });

        $('#btnTemplateExcel').on('click', function () {
           
        });

        $('#btnImportExcel').on('click', function () {
            var fileUpload = $("#fileInputExcel").get(0);
            var files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();
            // Looping over all files and add it to FormData object  
            for (var i = 0; i < files.length; i++) {
                fileData.append("files", files[i]);
            }
            // Adding one more key to FormData object  
           // fileData.append('categoryId', $('#ddlCategoryIdImportExcel').combotree('getValue'));
            $.ajax({
                url: '../ProductType/ImportExcel',
                type: 'POST',
                data: fileData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    //console.log(response);
                    if (response.Success == true) {
                        if (response.Message == 'SUCCESS') {
                            $('#modal-import-excel').modal('hide');
                            loadData();
                            common.notify('Thêm mới thành công' + response.Data + ' bản ghi', 'success');
                        }
                        else {
                            $('#modal-import-excel').modal('hide');
                            loadData();
                            common.notify(response.Message, 'success');
                            window.location.href = response.Data;
                        }
                       

                      

                       
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }
                    common.stopLoading();

                },
                error: function () {
                    common.notify("Can not load ajax function ImportExcel", 'error');
                    common.stopLoading();
                }
            });
        });

        $('#btnExport').on('click', function () {
            ExportExcel();

        });
    }
    function loadData(isPageChanged) {
        $('.ckcheck').prop('checked', false);
        var template = $('#table-template').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../ProductType/GetAllPaging',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: i + 1,
                            PT_ID: item.PT_ID,
                            PT_MODEL: item.PT_MODEL,
                            PT_NAME: item.PT_NAME,
                        });
                      
                       
                       
                    });
                    if (render != '') {
                        $('#tbl-content').html(render);
                    }
                    common.wrapPaging(response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="6">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-content').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        $('#txtID').val('');
       // initTreeDropDownCategory('');

        $('#txtMaSP').val('');
        $('#txtTenSP').val('');
        $('#txtID').prop('disabled', false);
      

    }

    function SaveProductType() {
        if ($('#frmMaintainance').valid()) {
            var chekId = $('#txtID').val();
            if (chekId.length <= 5) {

                $.ajax({
                    type: "POST",
                    url: "../ProductType/SaveEntity",
                    data: {
                        Id: $('#txtID').val(),
                        Code: $('#txtMaSP').val(),
                        Name: $('#txtTenSP').val()
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Data.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            $('#modal-add-edit').modal('hide');
                            resetFormMaintainance();

                            common.stopLoading();
                            loadData(true);
                        }
                        else {
                            if (response.Data.Message == 'CHECK') {
                                common.notify('ID đã trùng. Vui lòng nhập ID khác', 'error');
                            }
                            else {
                                common.notify(response.Message, 'error');
                            }
                        }
                     
                    },
                    error: function () {
                        common.notify('Has an error in save product progress', 'error');
                        common.stopLoading();
                    }
                });
            }
            else {
                common.notify('ID không được quá 5 ký tự', 'error');
            }
        }
    }

    function GetProductTypeById(Id) {
                $.ajax({
                    type: "GET",
                    url: "../ProductType/GetProductTypeById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        //console.log(response);
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            $('#txtID').val(data.PT_ID);
                            $('#txtMaSP').val(data.PT_MODEL);
                            $('#txtTenSP').val(data.PT_NAME);
                            $('#modal-add-edit').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Data.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function (status) {
                        common.notify('Có lỗi xảy ra: ' + response.Data.Message, 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function UpdateProductType() {
        if ($('#frmMaintainance').valid()) {

        $.ajax({
            type: "POST",
            url: "../ProductType/UpdateEntity",
            data: {
                Id: $('#txtID').val(),
                Code: $('#txtMaSP').val(),
                Name: $('#txtTenSP').val()
            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Data.Success = true) {
                    common.notify('Cập nhật thành công', 'success');

                    $('#modal-add-edit').modal('hide');
                    resetFormMaintainance();

                   
                    loadData(true);
                }
                else {
                    common.notify(response.Data.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateEntity', 'error');
                common.stopLoading();
            }
        });
        }
    }

    function DeleteProductType() {
        $.ajax({
            type: "POST",
            url: "../ProductType/Delete",
            data: { Id: checkxoa },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');
                   
                    loadData(true);
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function Delete', 'error');
                common.stopLoading();
            }
        });
    }

    function ExportExcel() {
        $.ajax({
            url: '../ProductType/ExportExcel',
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
}