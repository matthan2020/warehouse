﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class PalletController : Controller
    {
        private IPalletRepositories _palletRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PalletController(IPalletRepositories palletRepositories, IHostingEnvironment hostingEnvironment)
        {
            _palletRepositories = palletRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPallet(string status, string fromDate, string toDate, string keyword, int page, int pageSize)
        {
            var model = await _palletRepositories.GetAllPallet(status, fromDate, toDate,keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SavePallet( int number, string des)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _palletRepositories.SavePallet(UserName, number,  des);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
        [HttpGet]
        public async Task<IActionResult> GetPalletById(string id)
        {
            var model = await _palletRepositories.GetPalletById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePallet( string code, string des)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _palletRepositories.UpdatePallet(UserName, code, des);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
        [HttpPost]
        public async Task<IActionResult> DeletePallet(string Id)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _palletRepositories.DeletePallet(UserName,Id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
        [HttpGet]
        public async Task<IActionResult> LoadListDetail(string id)
        {
            var model = await _palletRepositories.LoadListDetail(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> ImportExcel(IList<IFormFile> files)
        {
            if (files != null && files.Count > 0)
            {
                var file = files[0];
                var filename = ContentDispositionHeaderValue
                                   .Parse(file.ContentDisposition)
                                   .FileName
                                   .Trim('"');

                var duoifile = filename.Substring(filename.Length - 5);
                if (duoifile == ".xlsx")
                {
                    string folder = _hostingEnvironment.WebRootPath + $@"\uploaded\excels";
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string filePath = Path.Combine(folder, filename);

                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    var model = await _palletRepositories.ImportExcel(filePath);

                    if (model.Success == true /*&& message.Substring(1,7) == "SUCCESS"*/)
                    {
                        if (model.Message.Length > 7)
                        {
                            return new OkObjectResult(new GenericResult(model.Success, model.Message.Substring(0, 7), model.Message.Substring(7, model.Message.Length - 7)));
                        }
                        else
                        {
                            string sWebRootFolder = _hostingEnvironment.WebRootPath;
                            string directory = Path.Combine(sWebRootFolder, "export-files", "PRODUCT");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            string sFileName = $"Product_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
                            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/PRODUCT/{sFileName}";
                            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateError", "ProductError.xlsx");
                            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
                            if (fileName.Exists)
                            {
                                fileName.Delete();

                                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                            }
                            int b = 0;
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["PRODUCT"];
                                    int i = 2;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = item.SO_DONG;
                                        workSheet.Cells[i, 2].Value = item.LOI;
                                        workSheet.Cells[i, 3].Value = item.WR_PR_TYPE;
                                        workSheet.Cells[i, 4].Value = item.WR_PR_CODE;
                                        workSheet.Cells[i, 5].Value = item.WR_PR_MODEL;
                                        workSheet.Cells[i, 6].Value = item.WR_PR_NAME;
                                        workSheet.Cells[i, 7].Value = item.WR_PR_NAME_VN;
                                        workSheet.Cells[i, 8].Value = item.WR_PR_CONTACT;
                                        workSheet.Cells[i, 9].Value = item.WR_PR_DESCRIPTION;
                                        i = i + 1;
                                        b = b + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(true, "Có " + model.Message + " dữ liệu thành công và " + b + " dữ liệu thất bại. Chi tiết thất bạo xem ở file dowload về", fileUrl));
                        }

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, ""));
                    }
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "File không đúng định dạng .xlsx"));
                }

            }
            else
            {
                return new OkObjectResult(new GenericResult(false, "Không có file để import"));
            }
        }

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string status, string fromDate, string toDate, string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "PALLET");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"Pallet_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/PALLET/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportPallet.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }
           
                    var model = await _palletRepositories.ExportExcel(status, fromDate, toDate,keyword);

                    if (model.Success == true && model.Message == "SUCCESS")
                    {
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["PALLET"];
                                    int i = 6;
                        
                                    foreach (var item in model.Items)
                                    {
                            if (item.STATUS == 0)
                            {
                                item.WR_STATUS = "Chưa sử dụng";
                            }
                            else
                            {
                                if (item.STATUS == 3)
                                {
                                    item.WR_STATUS = "Đã xóa";
                                }
                                else
                                {
                                    item.WR_STATUS = "Đã sử dụng";
                                }
                            }
                                        workSheet.Cells[i, 1].Value = i-5;
                                        workSheet.Cells[i, 2].Value = item.CODE;
                                        workSheet.Cells[i, 3].Value = item.DESCRIPTION;
                                        workSheet.Cells[i, 4].Value = item.WR_STATUS;
                                        workSheet.Cells[i, 5].Value = item.CREATE_DATE;
                                        workSheet.Cells[i, 6].Value = item.CREATE_USER;
                            i = i + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));
                        

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }



        }

       
    }
}
