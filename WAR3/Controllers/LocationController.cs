﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class LocationController : Controller
    {
        private ILocationRepositories _locationRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public LocationController(ILocationRepositories locationRepositories, IHostingEnvironment hostingEnvironment)
        {
            _locationRepositories = locationRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllLocation(string keyword, int page, int pageSize)
        {
            var model = await _locationRepositories.GetAllLocation(keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveLocation( string code,  string name)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data1 = await _locationRepositories.SaveLocation(UserName,code, name);
            return new OkObjectResult(new GenericResult(data1.Success, data1.Message));
        }
        [HttpGet]
        public async Task<IActionResult> GetLocationById(string id)
        {
            var model = await _locationRepositories.GetLocationById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateLocation(string code, string name)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model = await _locationRepositories.UpdateLocation(UserName,code,  name);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteLocation(string Id)
        {

            var UserName = Request.Cookies["USER_NAME"];
            var model = await _locationRepositories.DeleteLocation(UserName,Id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpPost]
        public async Task<IActionResult> ImportExcel(IList<IFormFile> files)
        {
            if (files != null && files.Count > 0)
            {
                var file = files[0];
                var filename = ContentDispositionHeaderValue
                                   .Parse(file.ContentDisposition)
                                   .FileName
                                   .Trim('"');

                var duoifile = filename.Substring(filename.Length - 5);
                if (duoifile == ".xlsx")
                {
                    string folder = _hostingEnvironment.WebRootPath + $@"\uploaded\excels";
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string filePath = Path.Combine(folder, filename);

                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    var model = await _locationRepositories.ImportExcel(filePath);

                    if (model.Success == true /*&& message.Substring(1,7) == "SUCCESS"*/)
                    {
                        if (model.Message.Length > 7)
                        {
                            return new OkObjectResult(new GenericResult(model.Success, model.Message.Substring(0, 7), model.Message.Substring(7, model.Message.Length-7)));
                        }
                        else
                        {
                            string sWebRootFolder = _hostingEnvironment.WebRootPath;
                            string directory = Path.Combine(sWebRootFolder, "export-files","LOCATION");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            string sFileName = $"LocationError_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
                            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/LOCATION/{sFileName}";
                            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateError", "LocationError.xlsx");
                            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
                            if (fileName.Exists)
                            {
                                fileName.Delete();

                                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                            }
                            int b = 0;
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["LOCATION"];
                                    int i = 2;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = item.SO_DONG;
                                        workSheet.Cells[i, 2].Value = item.LOI;
                                        workSheet.Cells[i, 3].Value = item.LC_CODE;
                                        workSheet.Cells[i, 4].Value = item.LC_NAME;
                                        workSheet.Cells[i, 5].Value = item.LCSTATUS;
                                        i = i + 1;
                                        b = b + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(true, "Có " + model.Message + " dữ liệu thành công và " + b + " dữ liệu thất bại. Chi tiết thất bạo xem ở file dowload về", fileUrl));
                        }

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }


                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "File không đúng định dạng .xlsx"));
                }

            }
            else
            {
                return new OkObjectResult(new GenericResult(false, "Không có file để import"));
            }
        }

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "LOCATION");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"Location_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/LOCATION/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportLocation.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }
           
                    var  model = await _locationRepositories.ExportExcel(keyword);

                    if (model.Success == true && model.Message == "SUCCESS")
                    {
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["LOCATION"];
                                    int i = 6;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = i-5;
                                        workSheet.Cells[i, 2].Value = item.LC_CODE;
                                        workSheet.Cells[i, 3].Value = item.LC_NAME;
                                        workSheet.Cells[i, 4].Value = item.LC_UPDATEUSER;
                                        workSheet.Cells[i, 5].Value = item.LC_UPDATEDATE;
                                        workSheet.Cells[i, 6].Value = item.LC_CREATEDATE;
                                        workSheet.Cells[i, 7].Value = item.LC_CREATEUSER;
                            i = i + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));
                        

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }



        }

        [HttpGet]
        public async Task<IActionResult> GetAllLocationNoPage()
        {
            var model = await _locationRepositories.ExportExcel(null);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
    }
}
