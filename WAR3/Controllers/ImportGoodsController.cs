﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.Implementation.Repositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class ImportGoodsController : Controller
    {
        private IImportGoodsRepositories _importGoodsRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ImportGoodsController(IImportGoodsRepositories importGoodsRepositories,  IHostingEnvironment hostingEnvironment)
        {
            _importGoodsRepositories = importGoodsRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
             ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }
        public IActionResult AddImportGoods()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        public IActionResult AddPallet(int ID,string TYPE)
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            ViewData["ID"] = ID;
                ViewData["TYPE"] = TYPE;

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllImportGoods(string status,string fromDate,string toDate, string keyword, int page, int pageSize)
        {
            var model = await _importGoodsRepositories.GetAllImportGoods(status, fromDate, toDate,keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveImportGoods(string licensePlates, string licenseName, string fromDate, string des, IList<IFormFile> files )
        {
           
            if (files != null && files.Count > 0)
            {
                var file = files[0];
                var filename = ContentDispositionHeaderValue
                                   .Parse(file.ContentDisposition)
                                   .FileName
                                   .Trim('"');
                var duoifile = filename.Substring(filename.Length - 5);
                if (duoifile == ".xlsx")
                {
                    string folder = _hostingEnvironment.WebRootPath + $@"\uploaded\excelImportGoods";
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }

                    //string folder1 = _hostingEnvironment.WebRootPath + $@"\uploaded\excelDownloadGoods";
                    //if (!Directory.Exists(folder1))
                    //{
                    //    Directory.CreateDirectory(folder1);
                    //}
                    string filePath = Path.Combine(folder, filename.Substring(0, filename.Length - 5) + "_"+DateTime.Now.ToString("yyyyMMddHHmmss")+".xlsx");

                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    var UserName = Request.Cookies["USER_NAME"];
                   var model= await _importGoodsRepositories.SaveImportGoods(UserName,licensePlates, licenseName, fromDate, des, filePath);
                    if (model.Success == true /*&& message.Substring(1,7) == "SUCCESS"*/)
                    {
                        if (model.Message.Length > 7)
                        {
                            return new OkObjectResult(new GenericResult(model.Success, model.Message.Substring(0, 7), model));
                        }
                        else
                        {
                            string sWebRootFolder = _hostingEnvironment.WebRootPath;
                            string directory = Path.Combine(sWebRootFolder, "export-files", "IMPORT_GOODS");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            string sFileName = $"ImportGoods_{DateTime.Now:yyyyMMddHHmmss}.xlsx";
                            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/IMPORT_GOODS/{sFileName}";
                            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateError", "ImportGoodsError.xlsx");
                            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
                            if (fileName.Exists)
                            {
                                fileName.Delete();

                                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                            }
                            int b = 0;
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["IMPORT_GOODS"];
                                    int i = 2;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = item.SO_DONG;
                                        workSheet.Cells[i, 2].Value = item.LOI;
                                        workSheet.Cells[i, 3].Value = item.IG_LICENSE_PLATES;
                                        workSheet.Cells[i, 4].Value = item.IG_ID;
                                        workSheet.Cells[i, 5].Value = item.IG_DESCRIPTION;
                                        i = i + 1;
                                        b = b + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(true, "Import dữ liệu thất bại. Chi tiết thất bạo xem ở file dowload về", fileUrl));
                        }

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, model.Message));
                    }
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "File không đúng định dạng .xlsx"));
                }
                   
            }
            else
            {
                return new OkObjectResult(new GenericResult(false, "Không có file để import"));
               
            }
               
        }

        [HttpPost]
        public async Task<IActionResult> SaveImportGoodsTC(string licensePlates, string licenseName, string fromDate, string des, List<MapProductGoods> items )
        {
            
            //JsonObject root = Windows.Data.Json.JsonValue.Parse(items).GetObject();
            var UserName = Request.Cookies["USER_NAME"];

            var model = await _importGoodsRepositories.SaveImportGoods(UserName, licensePlates, licenseName, fromDate, des, null);
            if (model.Success == true)
            {
                foreach(var item in items)
                {
                    await _importGoodsRepositories.SaveImportGoodsProduct(UserName, model.Id, item.MPG_PRODUCT, item.MPG_NUMBER);
                }
                //return new OkObjectResult(new GenericResult(model1.Success, model1.Message, model1));
            }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, model));
            
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMapGoodsProduct(string idGoods,  int page, int pageSize)
        {
            var model = await _importGoodsRepositories.GetAllMapGoodsProduct(idGoods,  page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetAllGoodsById(string idGoods)
        {
            var model = await _importGoodsRepositories.GetAllGoodsById(idGoods);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ///  [HttpGet]
           [HttpGet]
        public async Task<IActionResult> GetAllMapGoodsPallet(string idGoods, int page, int pageSize)
        {
            var model = await _importGoodsRepositories.GetAllMapGoodsPallet(idGoods, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetProductByGoods(string idGoods)
        {
            var model = await _importGoodsRepositories.GetProductByGoods(idGoods);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetPallet()
        {
            var model = await _importGoodsRepositories.GetPallet();
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SavePalletProduct(int id, string CodePallet, string CodeProduct, int NumberPallet)
        {

            //JsonObject root = Windows.Data.Json.JsonValue.Parse(items).GetObject();
            var UserName = Request.Cookies["USER_NAME"];

            var model =await _importGoodsRepositories.SavePalletProduct(UserName, id, CodePallet, CodeProduct, NumberPallet);
            
            return new OkObjectResult(new GenericResult(model.Success, model.Message));




        }
        [HttpPost]
        public async Task<IActionResult> DeletePallet(string Id)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model= await _importGoodsRepositories.DeletePallet(UserName,Id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpPost]
        public async Task<IActionResult> UpdateImportGoods(int idGoods,string licensePlates, string licenseName, string fromDate, string des)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model=await _importGoodsRepositories.UpdateImportGoods(UserName, idGoods,licensePlates, licenseName, fromDate, des);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProduct(string Id,string IdProduct)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model=await _importGoodsRepositories.DeleteProduct(UserName,Id, IdProduct);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }

        [HttpGet]
        public async Task<IActionResult> GetMapProducttById(string id,string codeProducct)
        {
            var model = await _importGoodsRepositories.GetMapProducttById(id, codeProducct);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveMapProduct(int id,string codeProduct, int so_luong)
        {
            var UserName = Request.Cookies["USER_NAME"];
            //await _importGoodsRepositories.SaveImportGoodsProduct(UserName, model.Id, item.MPG_PRODUCT, item.MPG_NUMBER);
            var model= await _importGoodsRepositories.SaveMapProduct(UserName, id, codeProduct, so_luong);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateMapProduct(int id,string codeProductCu, string codeProduct, int so_luong)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model=await _importGoodsRepositories.UpdateMapProduct(UserName, id, codeProductCu, codeProduct, so_luong);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpGet]
        public async Task<IActionResult> GetMapPalletById(string id)
        {
            var model = await _importGoodsRepositories.GetMapPalletById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateMapPallet(int id, int idPallet, string CodePallet, string CodeProduct, int NumberPallet,string checkcapnhat)
        {

            //JsonObject root = Windows.Data.Json.JsonValue.Parse(items).GetObject();
            var UserName = Request.Cookies["USER_NAME"];

            var model = await _importGoodsRepositories.UpdateMapPallet(UserName, id, idPallet,CodePallet, CodeProduct, NumberPallet, checkcapnhat);

            return new OkObjectResult(new GenericResult(model.Success, model.Message));




        }



        [HttpPost]
        public async Task<IActionResult> DeleteImportGoods(string Id)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model = await _importGoodsRepositories.DeleteImportGoods(UserName,Id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
     

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string status, string fromDate, string toDate, string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "IMPORTGOODS");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"ImportGoods_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/IMPORTGOODS/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportGoods.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }
           
                    var model = await _importGoodsRepositories.ExportExcel(status, fromDate, toDate,keyword);

                    if (model.Success == true && model.Message == "SUCCESS")
                    {
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["GOODS"];
                        ExcelWorksheet workSheet1 = package.Workbook.Worksheets["MAP_PRODUCT"];
                        ExcelWorksheet workSheet2 = package.Workbook.Worksheets["MAP_PALLET"];
                        int i = 6;
                        int j = 6;
                        int k = 6;
                                    foreach (var item in model.Items)
                                    {
                            if (item.PHAN_BIET == 0)
                            {
                               
                           
                                        workSheet.Cells[i, 1].Value = i - 5;
                                workSheet.Cells[i, 2].Value = item.IG_ID;
                                workSheet.Cells[i, 3].Value = item.IG_LICENSE_PLATES;
                                workSheet.Cells[i, 4].Value = item.IG_LICENSE_NAME;
                                workSheet.Cells[i, 5].Value = item.IG_GOODS_DATE;
                                workSheet.Cells[i, 6].Value = item.IG_STATUS == '2' ? "Chưa hoàn thành" : "Hoàn thành";
                                workSheet.Cells[i, 7].Value = item.IG_FILE;
                                workSheet.Cells[i, 8].Value = item.IG_DESCRIPTION;
                                workSheet.Cells[i, 9].Value = item.IG_CREATEDATE;
                                workSheet.Cells[i, 10].Value = item.IG_CREATEUSER;
                                i = i + 1;
                            }
                            if(item.PHAN_BIET == 1)
                            {
                                workSheet1.Cells[j, 1].Value = j - 5;
                                workSheet1.Cells[j, 2].Value = item.IG_ID;
                                workSheet1.Cells[j, 3].Value = item.IG_LICENSE_NAME;
                                workSheet1.Cells[j, 4].Value = item.IG_STATUS;
                                workSheet1.Cells[j, 5].Value = item.SO_LUONG;
                                workSheet1.Cells[j, 6].Value = item.IG_CREATEDATE;
                                workSheet1.Cells[j, 7].Value = item.IG_CREATEUSER;
                                j = j + 1;
                            }
                            if (item.PHAN_BIET == 2)
                            {
                                workSheet2.Cells[k, 1].Value = k - 5;
                                workSheet2.Cells[k, 2].Value = item.IG_ID;
                                workSheet2.Cells[k, 3].Value = item.IG_LICENSE_NAME;
                                workSheet2.Cells[k, 4].Value = item.IG_LICENSE_PLATES;
                                workSheet2.Cells[k, 5].Value = item.IG_STATUS;
                                workSheet2.Cells[k, 6].Value = item.IG_CREATEDATE;
                                workSheet2.Cells[k, 7].Value = item.IG_CREATEUSER;
                                k = k + 1;
                            }

                        }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(model.Success,  model.Message , fileUrl));
                        

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }



        }
    }
}
