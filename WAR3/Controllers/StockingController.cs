﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class StockingController : Controller
    {
        private IStockingRepositories _stockingRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public StockingController(IStockingRepositories stockingRepositories, IHostingEnvironment hostingEnvironment)
        {
            _stockingRepositories = stockingRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStocking( string fromDate, string toDate, string keyword, int page, int pageSize)
        {
            var model = await _stockingRepositories.GetAllStocking( fromDate, toDate,keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> GetStockingById(string id)
        {
            var model = await _stockingRepositories.GetStockingById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStocking( int id, string location)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _stockingRepositories.UpdateStocking(UserName, id, location);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
       

       
    }
}
