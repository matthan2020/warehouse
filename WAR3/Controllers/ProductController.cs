﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepositories _poductRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;

        public ProductController(IProductRepositories productRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory)
        {
            _poductRepositories = productRepositories;
            _hostingEnvironment = hostingEnvironment; ;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProduct(string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<ProductViewModel>>(
                $"api/Product/GetProductPage?pageIndex={page}" +
                $"&pageSize={pageSize}" +
                $"&keyword={keyword}");
           // return data;
            //var model = await _poductRepositories.GetAllProduct(keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data)); //new OkObjectResult(model);
        }
        protected async Task<TResponse> GetAsync<TResponse>(string url)
        {


            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");
            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            return JsonConvert.DeserializeObject<TResponse>(body);
        }
        [HttpPost]
        public async Task<IActionResult> SaveProduct(string productType, string code, string model, string name, string nameVN, string contact, string des)
        {
            var UserName = Request.Cookies["USER_NAME"];
            ProductViewModel data = new ProductViewModel();

            data.WR_PR_TYPE = productType;
            data.WR_PR_CODE = code;
            data.WR_PR_MODEL = model;
            data.WR_PR_NAME = name;
            data.WR_PR_NAME_VN = nameVN;
            data.WR_PR_CONTACT = contact;
            data.WR_PR_DESCRIPTION = des;
            data.WR_PR_CREATEUSER = UserName;
            var data1=await _poductRepositories.SaveProduct( data);
            return new OkObjectResult(new GenericResult(data1.Success, data1.Message));
        }
        [HttpGet]
        public async Task<IActionResult> GetProductById(string id)
        {
            var model = await _poductRepositories.GetProductById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProduct(string productType, string code, string model, string name, string nameVN, string contact, string des)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _poductRepositories.UpdateProduct(UserName,productType, code, model, name, nameVN, contact, des);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProduct(string Id)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _poductRepositories.DeleteProduct(UserName,Id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));
        }
        [HttpPost]
        public async Task<IActionResult> ImportExcel(IList<IFormFile> files)
        {
            if (files != null && files.Count > 0)
            {
                var file = files[0];
                var filename = ContentDispositionHeaderValue
                                   .Parse(file.ContentDisposition)
                                   .FileName
                                   .Trim('"');

                var duoifile = filename.Substring(filename.Length - 5);
                if (duoifile == ".xlsx")
                {
                    string folder = _hostingEnvironment.WebRootPath + $@"\uploaded\excels";
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string filePath = Path.Combine(folder, filename);

                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    var model = await _poductRepositories.ImportExcel(filePath);

                    if (model.Success == true /*&& message.Substring(1,7) == "SUCCESS"*/)
                    {
                        if (model.Message.Length > 7)
                        {
                            return new OkObjectResult(new GenericResult(model.Success, model.Message.Substring(0, 7), model.Message.Substring(7, model.Message.Length - 7)));
                        }
                        else
                        {
                            string sWebRootFolder = _hostingEnvironment.WebRootPath;
                            string directory = Path.Combine(sWebRootFolder, "export-files", "PRODUCT");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            string sFileName = $"Product_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
                            string fileUrl = $"{Request.Scheme}://{Request.Host}/export-files/PRODUCT/{sFileName}";
                            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateError", "ProductError.xlsx");
                            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
                            if (fileName.Exists)
                            {
                                fileName.Delete();

                                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                            }
                            int b = 0;
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["PRODUCT"];
                                    int i = 2;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = item.SO_DONG;
                                        workSheet.Cells[i, 2].Value = item.LOI;
                                        workSheet.Cells[i, 3].Value = item.WR_PR_TYPE;
                                        workSheet.Cells[i, 4].Value = item.WR_PR_CODE;
                                        workSheet.Cells[i, 5].Value = item.WR_PR_MODEL;
                                        workSheet.Cells[i, 6].Value = item.WR_PR_NAME;
                                        workSheet.Cells[i, 7].Value = item.WR_PR_NAME_VN;
                                        workSheet.Cells[i, 8].Value = item.WR_PR_CONTACT;
                                        workSheet.Cells[i, 9].Value = item.WR_PR_DESCRIPTION;
                                        i = i + 1;
                                        b = b + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(true, "Có " + model.Message + " dữ liệu thành công và " + b + " dữ liệu thất bại. Chi tiết thất bạo xem ở file dowload về", fileUrl));
                        }

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, ""));
                    }
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "File không đúng định dạng .xlsx"));
                }

            }
            else
            {
                return new OkObjectResult(new GenericResult(false, "Không có file để import"));
            }
        }

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "PRODUCT");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"Product_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/PRODUCT/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportProduct.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }
           
                    var model = await _poductRepositories.ExportExcel(keyword);

                    if (model.Success == true && model.Message == "SUCCESS")
                    {
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["PRODUCT"];
                                    int i = 6;
                                    foreach (var item in model.Items)
                                    {
                                        workSheet.Cells[i, 1].Value = i-5;
                                        workSheet.Cells[i, 2].Value = item.WR_PR_TYPE;
                                        workSheet.Cells[i, 3].Value = item.WR_PR_CODE;
                                        workSheet.Cells[i, 4].Value = item.WR_PR_MODEL;
                                        workSheet.Cells[i, 5].Value = item.WR_PR_NAME;
                                        workSheet.Cells[i, 6].Value = item.WR_PR_NAME_VN;
                                        workSheet.Cells[i, 7].Value = item.WR_PR_CONTACT;
                                        workSheet.Cells[i, 8].Value = item.WR_PR_DESCRIPTION;
                                        workSheet.Cells[i, 9].Value = item.WR_PR_CREATEDATE;
                                        workSheet.Cells[i, 10].Value = item.WR_PR_CREATEUSER;
                            i = i + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));
                        

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }



        }

        [HttpPost]
        public async Task<IActionResult> GetAllProduct(string keyword)
        {

            var model = await _poductRepositories.ExportExcel(keyword);


            return new OkObjectResult(new GenericResult(model.Success, model.Message, model));



        }
    }
}
