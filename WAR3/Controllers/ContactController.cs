﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class ContactController : Controller
    {
        private IContactRepositories _contactRepositories;
        private IProductRepositories _poductRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ContactController(IContactRepositories contactRepositories, IProductRepositories productRepositories, IHostingEnvironment hostingEnvironment)
        {
            _contactRepositories = contactRepositories;
            _poductRepositories = productRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllContact(string keyword, int page, int pageSize)
        {
            var model = await _contactRepositories.GetAllContact(keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveContact(string type, string code, string name, string company, string address, string email,string phone,string receiverUser,string receiverDate, string des,string createDate)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model = await _contactRepositories.SaveContact(UserName,type, code,  name, company, address, email, phone, receiverUser, receiverDate, des, createDate);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpGet]
        public async Task<IActionResult> GetContactById(string id)
        {
            var model = await _contactRepositories.GetContactById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateContact(string type, string code, string name, string company, string address, string email, string phone, string receiverUser, string receiverDate, string des, string createDate)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model = await _contactRepositories.UpdateContact(type, code, name, company, address, email, phone, receiverUser, receiverDate, des, createDate, UserName);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteContact(string Id)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var model = await _contactRepositories.DeleteContact(UserName,Id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message));
        }
     

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "CONTACT");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"Contact_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/CONTACT/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportContact.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }

            var model = await _contactRepositories.ExportExcel(keyword);

                    if (model.Success == true && model.Message == "SUCCESS")
                    {
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["CONTACT"];
                                    int i = 6;
                        //var phone1 = "";
                                    foreach (var item in model.Items)
                                    {
                            //if (item.CT_PHONE != "" ) {
                            //    phone1 = item.CT_PHONE.ToString();
                            //}
                            //else
                            //{
                            //    phone1 = "";
                            //}
                                        workSheet.Cells[i, 1].Value = i-5;
                                        workSheet.Cells[i, 2].Value = item.CT_CODE;
                                        workSheet.Cells[i, 3].Value = item.CT_TYPE;
                                        workSheet.Cells[i, 4].Value = item.CT_NAME;
                                        workSheet.Cells[i, 5].Value = item.CT_COMPANY;
                                        workSheet.Cells[i, 6].Value = item.CT_ADDRESS;
                                        workSheet.Cells[i, 7].Value = item.CT_EMAIL;
                                        workSheet.Cells[i, 8].Value = item.CT_PHONE;
                                        workSheet.Cells[i, 9].Value = item.CT_RECEIVER_USER;
                                        workSheet.Cells[i, 10].Value = item.CT_DESCRIPTION;
                                        workSheet.Cells[i, 11].Value = item.CT_RECEIVER_DATE;
                                        workSheet.Cells[i, 12].Value = item.CT_CREATEDATE;
                                        workSheet.Cells[i, 13].Value = item.CT_CREATEUSER;
                            i = i + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));
                        

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(model.Success, model.Message));
                    }



        }
    }
}
