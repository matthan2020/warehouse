﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class ProductTypeController : Controller
    {
        private IProductTypeRepositories _poductTypeRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        private IContactRepositories _contactRepositories;
        public ProductTypeController(IProductTypeRepositories productTypeRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory,
            IContactRepositories contactRepositories)
        {
            _poductTypeRepositories = productTypeRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
            _contactRepositories = contactRepositories;
        }
        public IActionResult Index()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPaging(string keyword, int page, int pageSize)
        {
            // var model = _poductTypeRepositories.GetAllPaging( keyword, page, pageSize, out bool result, out string message);
            //return new OkObjectResult(new GenericResult(result, message, model)); //new OkObjectResult(model);
            //var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri("https://localhost:44336");
            //var response = await client.GetAsync (
            //   $"/api/ProductType/paging?pageIndex={page}" +
            //   $"&pageSize={pageSize}" +
            //   $"&keyword={keyword}");
            //var body = await response.Content.ReadAsStringAsync();
            ////var model= JsonConvert.DeserializeObject<ApiSuccessResult<PagedResult<ProductTypeViewModel>>>(body);



            //if (response.IsSuccessStatusCode)
            //{
            //    ProductTypeViewModel myDeserializedObjList = (ProductTypeViewModel)JsonConvert.DeserializeObject(body,
            //        typeof(ProductTypeViewModel));
            //    return new OkObjectResult(new GenericResult(true, "", myDeserializedObjList));
            //    //return myDeserializedObjList;
            //}
            //var model=JsonConvert.DeserializeObject<TResponse>(body);
            //return new OkObjectResult(new GenericResult(true, "", response)); //new OkObjectResult(model);
            var data = await GetAsync<PagedResultAPI<ProductTypeViewModel>>(
               $"api/ProductType/paging?pageIndex={page}" +
               $"&pageSize={pageSize}" +
               $"&keyword={keyword}");
            return new OkObjectResult(new GenericResult(true, "", data));
        }
        protected async Task<TResponse> GetAsync<TResponse>(string url)
        {
           

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");
            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            return JsonConvert.DeserializeObject<TResponse>(body);
        }
        protected async Task<TResponse> PostAsync<TResponse>(string url,string json)
        {


            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");
           // var response = await client.GetAsync(url);
           // var body = await response.Content.ReadAsStringAsync();
          
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, httpContent);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            //var abc = JsonConvert.DeserializeObject<TResponse>(result);

            return JsonConvert.DeserializeObject<TResponse>(body);

           
        }
        [HttpPost]
        public async Task<IActionResult> SaveEntity(string Id,string Code,string Name)
        {
            //_poductTypeRepositories.SaveEntity(Id, Code, Name, out bool result, out string message);
            //return new OkObjectResult(new GenericResult(result, message));
            var UserName = Request.Cookies["USER_NAME"];
            List<GetProductTypeViewModel> data = new List<GetProductTypeViewModel>();

            data.Add(
       new GetProductTypeViewModel()
       {
           ID = Id,
           NAME = Name,
           CREATEUSER = UserName
       });
           
            var json = JsonConvert.SerializeObject(data[0]);

            var data1 = await PostAsync<PagedResultAPI<ProductTypeViewModel>>(
             $"api/ProductType/SaveProductType", json);
            return new OkObjectResult(new GenericResult(true, "", data1));




            //return new OkObjectResult(new GenericResult(response.IsSuccessStatusCode, response.ReasonPhrase));
        }
        [HttpGet]
        public async Task<IActionResult> GetProductTypeById(string id)
        {
            //var model = _poductTypeRepositories.GetProductTypeById(id, out bool result, out string message);
            //return new OkObjectResult(new GenericResult(result, message, model)); //new OkObjectResult(model);
            var data = await GetAsync<PagedResultAPI<ProductTypeViewModel>>(
               $"api/ProductType/GetProductTypeById?id={id}");
            return new OkObjectResult(new GenericResult(true, "", data));
        }
        [HttpPost]
        public async Task<IActionResult> UpdateEntity(string Id, string Code, string Name)
        {
            //_poductTypeRepositories.UpdateEntity(Id, Code, Name, out bool result, out string message);
            //return new OkObjectResult(new GenericResult(result, message));
            var UserName = Request.Cookies["USER_NAME"];
            List<GetProductTypeViewModel> data = new List<GetProductTypeViewModel>();

            data.Add(
       new GetProductTypeViewModel()
       {
           ID = Id,
           NAME = Name,
           CREATEUSER = UserName
       });

            var json = JsonConvert.SerializeObject(data[0]);

            var data1 = await PostAsync<PagedResultAPI<ProductTypeViewModel>>(
             $"api/ProductType/UpdateProductType", json);
            return new OkObjectResult(new GenericResult(true, "", data1));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string Id)
        {
            //_poductTypeRepositories.DeleteEntity(Id, out bool result, out string message);
            //return new OkObjectResult(new GenericResult(result, message));
            var UserName = Request.Cookies["USER_NAME"];
            List<GetProductTypeViewModel> data = new List<GetProductTypeViewModel>();

            data.Add(
       new GetProductTypeViewModel()
       {
           ID = Id,
           CREATEUSER = UserName
       });

            var json = JsonConvert.SerializeObject(data[0]);

            var data1 = await PostAsync<PagedResultAPI<ProductTypeViewModel>>(
             $"api/ProductType/DeleteProductType", json);
            
            return new OkObjectResult(new GenericResult(data1.Success, data1.Message, data1));
        }

        [HttpPost]
        public IActionResult ImportExcel(IList<IFormFile> files)
        {
            if (files != null && files.Count > 0)
            {
                var file = files[0];
                var filename = ContentDispositionHeaderValue
                                   .Parse(file.ContentDisposition)
                                   .FileName
                                   .Trim('"');

                var duoifile = filename.Substring(filename.Length-5);
                if (duoifile == ".xlsx")
                {
                    string folder = _hostingEnvironment.WebRootPath + $@"\uploaded\excels";
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    string filePath = Path.Combine(folder, filename);

                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    List<ProductTypeViewModel> model = _poductTypeRepositories.ImportExcel(filePath, out bool result, out string message);
                   
                    if (result==true /*&& message.Substring(1,7) == "SUCCESS"*/)
                    {
                        if (message.Length > 7)
                        {
                            return new OkObjectResult(new GenericResult(result, message.Substring(1, 7), message.Substring(7, message.Length)));
                        }
                        else
                        {
                            string sWebRootFolder = _hostingEnvironment.WebRootPath;
                            string directory = Path.Combine(sWebRootFolder, "export-files");
                            string directory1 = Path.Combine(sWebRootFolder, "Template");
                            if (!Directory.Exists(directory))
                            {
                                Directory.CreateDirectory(directory);
                            }
                            string sFileName = $"Product_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
                            string fileUrl = $"{Request.Scheme}://{Request.Host}/export-files/{sFileName}";
                            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateError", "ProductTypeError.xlsx");
                            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
                            if (fileName.Exists)
                            {
                                fileName.Delete();
                               
                                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                               
                            }
                            int b = 0;
                            using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    // add a new worksheet to the empty workbook
                                    // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["PRODUCT_TYPE"];
                                    int i = 2;
                                    foreach (var item in model)
                                    {
                                        workSheet.Cells[i, 1].Value = item.SO_DONG;
                                        workSheet.Cells[i, 2].Value = item.PT_ID;
                                        workSheet.Cells[i, 3].Value = item.PT_NAME;
                                        workSheet.Cells[i, 4].Value = item.LOI;
                                        i = i + 1;
                                        b = b + 1;
                                    }
                                    package.SaveAs(fileName);
                                    //fileName.CopyTo(package);

                                }
                            }
                            return new OkObjectResult(new GenericResult(true, "Có " + message + " dữ liệu thành công và "+b+" dữ liệu thất bại. Chi tiết thất bạo xem ở file dowload về", fileUrl));
                        }

                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(result, message));
                    }
                  
                   
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "File không đúng định dạng .xlsx"));
                }
                
            }
            else
            {
                return new OkObjectResult(new GenericResult(false, "Không có file để import"));
            }
        }

        [HttpGet]
        public async Task<IActionResult> ExportExcel(string keyword)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "PRODUCT_TYPE");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"ProductType_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/WMS/export-files/PRODUCT_TYPE/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportProductType.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }
            var model = await GetAsync<PagedResultAPI<ProductTypeViewModel>>(
              $"api/ProductType/GetAllProductType?keyword={keyword}");
           // List<ContactViewModel> model = _contactRepositories.ExportExcel(keyword, out bool result, out string message);

            if (model.Success == true && model.Message == "SUCCESS")
            {
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // add a new worksheet to the empty workbook
                        // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                        ExcelWorksheet workSheet = package.Workbook.Worksheets["PRODUCT_TYPE"];
                        int i = 6;
                        foreach (var item in model.Items)
                        {
                            
                            workSheet.Cells[i, 1].Value = i - 5;
                            workSheet.Cells[i, 2].Value = item.PT_ID;
                            workSheet.Cells[i, 3].Value = item.PT_NAME;
                            i = i + 1;
                        }
                        package.SaveAs(fileName);
                        //fileName.CopyTo(package);

                    }
                }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));


            }
            else
            {
                return new OkObjectResult(new GenericResult(model.Success, model.Message));
            }



        }
        [HttpGet]
        public async Task<IActionResult> GetProductType()
        {
            var model = await GetAsync<PagedResultAPI<ProductTypeViewModel>>(
              $"api/ProductType/GetAllProductType?keyword={""}");
            //var model = _poductTypeRepositories.GetProductType( out bool result, out string message);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
    }
}
