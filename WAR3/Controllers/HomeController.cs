﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WAR3.Extensions;
using WAR3.Models;

namespace WAR3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewData["LOAD"] = Request.Cookies["LOAD"];

            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
               //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] ="0";
                //return View();
                return new RedirectResult("~/Login/Login");
            }
           // return View();
        }
      
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
