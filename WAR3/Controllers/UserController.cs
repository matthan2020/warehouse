﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using WAR3.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class UserController : Controller
    {
        private IUserRepositories _productService;
        private readonly IHttpClientFactory _httpClientFactory;

        public UserController(IUserRepositories productService, IHttpClientFactory httpClientFactory)
        {

            _productService = productService;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Account()
        {
            ViewData["NAME"] = Request.Cookies["NAME"];
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(User request)
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetUserByUserName()
        {
            var UserName =  Request.Cookies["USER_NAME"];
            var Password= Request.Cookies["PASSWORD"];

            //var model = _productService.GetUserByName(UserName, Password, out bool result, out string message);

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");
            var response = await client.GetAsync($"api/User/{UserName}/{Password}");
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                UserViewModel myDeserializedObjList = (UserViewModel)JsonConvert.DeserializeObject(body,
                    typeof(UserViewModel));
                Response.Cookies.Append("USER_NAME", myDeserializedObjList.USER_NAME);
                Response.Cookies.Append("PASSWORD", myDeserializedObjList.PASSWORD);
                Response.Cookies.Append("NAME", myDeserializedObjList.NAME);
                //if(myDeserializedObjList.)
                return new ObjectResult(new GenericResult(true, myDeserializedObjList));

            }
            return new ObjectResult(new GenericResult(false, response.IsSuccessStatusCode));

            //return new OkObjectResult(new GenericResult(result, message, model)); //new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveUpdateUser(GetUserViewModel request, string Name,string Company, string Address, string Email, string Phone)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var Password = Request.Cookies["PASSWORD"];
            //_productService.SaveUpdateUser(UserName, Password, Name, Company, Address, Email, Phone, out bool result, out string message);
            //if (result == true && message == "SUCCESS")
            //{

            //    Response.Cookies.Append("NAME", Name);
            //}
            //return new OkObjectResult(new GenericResult(result, message)); //new OkObjectResult(model);
            //List<GetUserViewModel> data = new List<GetUserViewModel>();
            //data.add
            //var url = "https://localhost:44336/api/User/UpdateUser";
            //WebRequest request = WebRequest.Create(url);
            //request.Credentials = CredentialCache.DefaultCredentials;
            //request.Method = "POST";
            //byte[] byteArray = Encoding.UTF8.GetBytes(json);

            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;


            //// request.ContentType = "application/json"
            //request.ContentType = "application/json";
            //request.ContentLength = byteArray.Length;
            //Stream dataStream1 = request.GetRequestStream();
            //dataStream1.Write(byteArray, 0, byteArray.Length);

            //WebResponse response = request.GetResponse();
            //Stream dataStream = response.GetResponseStream();
            //StreamReader reader = new StreamReader(dataStream);



            //// Dim soapEnvelopeXml As XmlDocument = New XmlDocument

            //// soapEnvelopeXml.LoadXml(kq)

            //// kq = reader.ReadA
            //reader.Close();
            //response.Close();
            List<GetUserViewModel> data = new List<GetUserViewModel> () ;

            data.Add(
       new GetUserViewModel()
       {
           USERNAME = UserName,
           NAME = Name,
           NAMECOMPANY = Company,
           ADDRESS = Address,
           EMAIL = Email,
           PHONE = Phone,
           CREATEUSER = UserName
       }) ;
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");


            var json = JsonConvert.SerializeObject(data[0]);
            
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync($"api/User/UpdateUser", httpContent);
            var result = await response.Content.ReadAsStringAsync();


            //var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri("https://localhost:44336");

            //var requestContent = new MultipartFormDataContent();

            //requestContent.Add(new StringContent(UserName), "username");
            //requestContent.Add(new StringContent(Name), "name");
            //requestContent.Add(new StringContent(Company), "namecompany");
            //requestContent.Add(new StringContent(Address), "address");
            //requestContent.Add(new StringContent(Email), "email");

            //requestContent.Add(new StringContent(Phone), "phone");

            //var response = await client.PostAsync($"/api/User/UpdateUser", requestContent);
            if (response.IsSuccessStatusCode == true)
            {
                Response.Cookies.Append("NAME", Name);
            }
            return new OkObjectResult(new GenericResult( response.IsSuccessStatusCode,response.ReasonPhrase));
            //return response.IsSuccessStatusCode;
        }

        [HttpPost]
        public async Task<IActionResult> SaveUpdatePass(string PassNew)
        {
            var UserName = Request.Cookies["USER_NAME"];
            List<GetUserViewModel> data = new List<GetUserViewModel>();

            data.Add(
       new GetUserViewModel()
       {
           USERNAME = UserName,
           PASSWORD= PassNew,
           CREATEUSER = UserName
       });
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");


            var json = JsonConvert.SerializeObject(data[0]);

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync($"api/User/UpdatePassWord", httpContent);
            var result = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode == true)
            {
               Response.Cookies.Append("PASSWORD", PassNew);
            }
            return new OkObjectResult(new GenericResult(response.IsSuccessStatusCode, response.ReasonPhrase));
            //_productService.SaveUpdatePass(UserName, PassNew, out bool result, out string message);
            //if (result == true && message == "SUCCESS")
            //{

            //    Response.Cookies.Append("PASSWORD", PassNew);
            //}
            //return new OkObjectResult(new GenericResult(result, message)); //new OkObjectResult(model);
        }
    }
}
