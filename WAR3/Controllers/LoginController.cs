﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.ExpressionGraph.FunctionCompilers;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class LoginController : Controller
    {
        
        private IUserRepositories _productService;
        private readonly IHttpClientFactory _httpClientFactory;
       // private readonly IConfiguration _configuration;

        public LoginController( IUserRepositories productService, IHttpClientFactory httpClientFactory/*, IConfiguration configuration*/)
        {
            
            _productService = productService;
            _httpClientFactory = httpClientFactory;
            //_configuration = configuration; ;
        }
        public IActionResult Login()
        {
            if (Request.Cookies["REMENBER"] == "true")
            {
                ViewData["USER_NAME"] = Request.Cookies["USER_NAME"];
                ViewData["PASSWORD"] = Request.Cookies["PASSWORD"];

            }
            if (Request.Cookies["LOAD"] != null)
            {
                Response.Cookies.Delete("LOAD");
            }
            return View();
        }

        [HttpPost]
        public async Task <IActionResult> Authen(string UserName,string Password)
        {
            if (Request.Cookies["USER_NAME"] != null)
            {
                Response.Cookies.Delete("USER_NAME");
            }
            if (Request.Cookies["PASSWORD"] != null)
            {
                Response.Cookies.Delete("PASSWORD");
            }
            if (Request.Cookies["NAME"] != null)
            {
                Response.Cookies.Delete("NAME");
            }
            if (Request.Cookies["LOAD"] != null)
            {
                Response.Cookies.Delete("LOAD");
            }
            ////var result1 = await _signInManager.PasswordSignInAsync(UserName, Password,false ,  lockoutOnFailure: false);
            //var model=_productService.GetUserByName(UserName, Password, out bool result, out string message);
            //if(result==true && message== "SUCCESS")
            //{
            //    Response.Cookies.Append("USER_NAME", model[0].USER_NAME);
            //    Response.Cookies.Append("PASSWORD", model[0].PASSWORD);
            //    Response.Cookies.Append("NAME", model[0].NAME);
            //    //ISession.SetString("USER_NAME", model[0].USER_NAME);
            //    //_session.SetString("PASSWORD", model[0].PASSWORD);
            //    //_session.SetString("NAME", model[0].NAME);
            //}
            //try
            //{
            //    var client = _httpClientFactory.CreateClient();
            //    client.BaseAddress = new Uri("https://localhost:44336");
            //    var response = await client.GetAsync($"/api/User/{UserName}/{Password}");
            //    var body = await response.Content.ReadAsStringAsync();
            //    if (response.IsSuccessStatusCode)
            //    {
            //        UserViewModel myDeserializedObjList = (UserViewModel)JsonConvert.DeserializeObject(body,
            //            typeof(UserViewModel));
            //        Response.Cookies.Append("USER_NAME", myDeserializedObjList.USER_NAME);
            //        Response.Cookies.Append("PASSWORD", myDeserializedObjList.PASSWORD);
            //        Response.Cookies.Append("NAME", myDeserializedObjList.NAME);
            //        //if(myDeserializedObjList.)
            //        return new ObjectResult(new GenericResult(true, myDeserializedObjList));

            //    }
            //    return new ObjectResult(new GenericResult(false, response.IsSuccessStatusCode));
            //}
            //catch(Exception ex)
            //{
            //    return new ObjectResult(new GenericResult(false, ex.Message));
            //}
            var data = await GetAsync<PagedResultAPI<UserViewModel>>(
               $"api/User/Login?userName={UserName}" +
               $"&passWord={Password}");
            if(data.Success==true && data.Message!= "NoData") 
                {
                Response.Cookies.Append("USER_NAME", data.Items[0].USER_NAME);
                Response.Cookies.Append("PASSWORD", data.Items[0].PASSWORD);
                Response.Cookies.Append("NAME", data.Items[0].NAME);
                Response.Cookies.Append("LOAD", "1");
            }
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

            //var result = _productService.Authenticate( UserName, Password);


        }
        protected async Task<TResponse> GetAsync<TResponse>(string url)
        {


            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.168/API/WMS_API/");
            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            return JsonConvert.DeserializeObject<TResponse>(body);
        }
        void POST(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType =   "application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    // got response
                    length = response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }




    }
}
