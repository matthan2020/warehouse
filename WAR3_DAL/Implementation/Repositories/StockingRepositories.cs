﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class StockingRepositories : BaseApiClient, IStockingRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public StockingRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<MapPalletProductViewModel>> GetAllStocking( string fromDate, string toDate, string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<MapPalletProductViewModel>>(
                 $"api/Stocking/GetAllStocking?pageIndex={page}" +
                $"&pageSize={pageSize}" +
                $"&keyword={keyword}" +
                $"&fromDate={fromDate}" +
                $"&toDate={toDate}");
            return data;
        }

      

        public async Task<PagedResultAPI<MapPalletProductViewModel>> GetStockingById(string id)
        {
            var data = await GetAsync<PagedResultAPI<MapPalletProductViewModel>>(
              $"api/ImportGoods/GetMapPalletById?id={id}");
            return data;
        }

        public async Task<PagedResultAPI<MapPalletProductViewModel>> UpdateStocking(string UserName, int id, string location)
        {
            MapPalletProductViewModel data = new MapPalletProductViewModel();

            data.MPP_ID = id;
            data.MPP_LOCATION = location;
            data.MPP_CREATEUSER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<MapPalletProductViewModel>>(
             $"api/Stocking/UpdateStocking", json);
            return data1;
        }

       
    }
}
