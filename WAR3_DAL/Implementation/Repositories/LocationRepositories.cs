﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class LocationRepositories : BaseApiClient,ILocationRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public LocationRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<LocationViewModel>> GetAllLocation(string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<LocationViewModel>>(
                 $"api/Location/GetAllLocationPage?pageIndex={page}" +
                 $"&pageSize={pageSize}" +
                 $"&keyword={keyword}");
            return data;
        }

        public async Task<PagedResultAPI<LocationViewModel>> SaveLocation(string UserName, string code,  string name)
        {
            PagedResultAPI<LocationViewModel> data1 = new PagedResultAPI<LocationViewModel>();
            try
            {
                LocationViewModel data = new LocationViewModel();

                data.LC_CODE = code;
                data.LC_NAME = name;
                data.LC_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<LocationViewModel>>(
                 $"api/Location/SaveLocation", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<LocationViewModel>> GetLocationById(string id)
        {
            var data = await GetAsync<PagedResultAPI<LocationViewModel>>(
              $"api/Location/GetLocationById?id={id}");
            return data;
        }

        public async Task<PagedResultAPI<LocationViewModel>> UpdateLocation(string UserName, string code,  string name)
        {
            LocationViewModel data = new LocationViewModel();

            data.LC_CODE = code;
            data.LC_NAME = name;
            data.LC_UPDATEUSER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<LocationViewModel>>(
             $"api/Location/UpdateLocation", json);
            return data1;
        }

        public async Task<PagedResultAPI<LocationViewModel>> DeleteLocation(string UserName, string Id)
        {
            LocationViewModel data = new LocationViewModel();

            data.LC_CODE = Id;
            data.LC_CREATEUSER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<LocationViewModel>>(
             $"api/Location/DeleteLocation", json);
            return data1;
        }

        public async Task<PagedResultAPI<LocationViewModel>> ImportExcel(string filePath)
        {
            PagedResultAPI<LocationViewModel> data = new PagedResultAPI<LocationViewModel>();
            List<LocationViewModel> dataItem = new List<LocationViewModel>();
            data.Success = true;
            try
            {
                int j = 0;
                int a = 0;
                var message = "";
                using (var package = new ExcelPackage(new FileInfo(filePath)))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    if(workSheet.Cells[1, 1].Value.ToString().Trim()!= "CODE" || workSheet.Cells[1, 2].Value.ToString().Trim() != "NAME" || workSheet.Cells[1, 3].Value.ToString().Trim() != "STATUS")
                    {
                        data.Success = false;
                        data.Message = "Template không đúng định dạng";
                        return data;
                    }
                    else
                    {
                        LocationViewModel product;
                        a = workSheet.Dimension.End.Row-1;
                        for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                        {
                            product = new LocationViewModel();
                            product.LC_CODE = workSheet.Cells[i, 1].Value == null ? string.Empty : workSheet.Cells[i, 1].Value.ToString();
                            product.LC_NAME = workSheet.Cells[i, 2].Value == null ? string.Empty : workSheet.Cells[i, 2].Value.ToString();
                           
                           
                            product.LC_STATUS = workSheet.Cells[i, 3].Value == null ? string.Empty : workSheet.Cells[i, 3].Value.ToString();

                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 1].Value.ToString()))
                            //{
                            //    product.WR_PR_TYPE = workSheet.Cells[i, 1].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 2].Value.ToString()))
                            //{
                            //    product.WR_PR_CODE = workSheet.Cells[i, 2].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 3].Value.ToString()))
                            //{
                            //    product.WR_PR_MODEL = workSheet.Cells[i, 3].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 4].Value.ToString()))
                            //{
                            //    product.WR_PR_NAME = workSheet.Cells[i, 4].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 5].Value.ToString()))
                            //{
                            //    product.WR_PR_NAME_VN = workSheet.Cells[i, 5].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 6].Value.ToString()))
                            //{
                            //    product.WR_PR_CONTACT = workSheet.Cells[i, 6].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 7].Value.ToString()))
                            //{
                            //    product.WR_PR_DESCRIPTION = workSheet.Cells[i, 7].Value.ToString();
                            //}
                            //AddImportExxcel(product, out result, out message);
                            var json = JsonConvert.SerializeObject(product);

                            var data1 = await PostAsync<PagedResultAPI<LocationViewModel>>(
                             $"api/Location/IULocation", json);
                            message = data1.Message;
                            if (data1.Success == false)
                            {
                                dataItem.Add(
        new LocationViewModel()
        {
            SO_DONG = i,
            LOI = message,
            LC_CODE = product.LC_CODE,
            LC_NAME = product.LC_NAME,
            LCSTATUS = product.LCSTATUS
        });
                            }
                            else
                            {
                                j = j + 1;
                            }
                            //_productRepository.Add(product);
                        }
                    }
                   
                }
                if (a != j)
                {
                    data.Items = dataItem;
                    data.Success = true;
                    data.Message = j.ToString();
                }
                else
                {
                    data.Message = message+j.ToString();
                }
                return data;
            }
            catch (Exception ex)
            {
                data.Success = false;
                data.Message = ex.Message;
                return data;
            }
        }
        public void AddImportExxcel(LocationViewModel product,  out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.IULocation";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Code", product.LC_CODE, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", product.LC_NAME, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Status", product.LCSTATUS, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public async Task<PagedResultAPI<LocationViewModel>> ExportExcel(string keyword)
        {
            var data = await GetAsync<PagedResultAPI<LocationViewModel>>(
               $"api/Location/GetAllLocationNoPage?keyword={keyword}");
            return data;
        }
    }
}
