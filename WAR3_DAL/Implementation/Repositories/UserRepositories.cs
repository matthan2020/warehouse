﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class UserRepositories: IUserRepositories

    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        //private readonly IHttpContextAccessor _httpContextAccessor;
        public UserRepositories(IHttpClientFactory httpClientFactory,
                   //IHttpContextAccessor httpContextAccessor,
                    IConfiguration configuration) 
        {
            ConnectedString = configuration.GetConnectionString("DefaultConnection");
            _configuration = configuration;
           //_httpContextAccessor = httpContextAccessor;
            _httpClientFactory = httpClientFactory;
        }
        public List<UserViewModel> GetUserByName(string UserName, string Password, out bool result, out string message)
        {
           
                result = true;
                message = "";
            SqlDataReader rdr = null;
            List<UserViewModel> data = new List<UserViewModel>();
            using (var oracleConnection = new SqlConnection(ConnectedString))
                {
                    oracleConnection.Open();
                //SqlCommand sql_cmnd = new SqlCommand("GetUserLogin", oracleConnection);
                //sql_cmnd.CommandType = CommandType.StoredProcedure;
                //sql_cmnd.Parameters.AddWithValue("@v_UserName", SqlDbType.NVarChar).Value = UserName;
                //sql_cmnd.Parameters.AddWithValue("@v_Password", SqlDbType.NVarChar).Value = Password;
               
                SqlCommand cmd = new SqlCommand("GetUserLogin", oracleConnection);

                //// 2. set the command object so it knows
                ////    to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                //// 3. add parameter to command, which
                ////    will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@v_UserName", UserName));
                cmd.Parameters.Add(new SqlParameter("@v_Password", Password));

                //// execute the command
                rdr = cmd.ExecuteReader();
                //    string query = "PKG_WR_USER.GetUserLogin";
                //    var dynamicParameters = new OracleDynamicParameters();
                //    //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                //    dynamicParameters.Add("v_UserName", UserName, OracleMappingType.NVarchar2, ParameterDirection.Input);
                //    dynamicParameters.Add("v_Password", Password, OracleMappingType.NVarchar2, ParameterDirection.Input);
                //dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                //dynamicParameters.Add("DATA", null, OracleMappingType.RefCursor, ParameterDirection.ReturnValue);

                try
                {
                    //sql_cmnd.ExecuteNonQuery();
                    while (rdr.Read())
                    {
                        var abc = rdr["USER_NAME"].ToString();
                        //Console.WriteLine("Product: {0,-35} Total: {1,2}", rdr["ProductName"], rdr["Total"]);
                    }
                    //data = SqlMapper.Query<UserViewModel>(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();
                    //message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (data.Count == 0 && result == true)
                        message = "NoData";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }

                oracleConnection.Close();

                return data;
            }
        }

        public void SaveUpdateUser(string UserName, string Password, string Name, string Company, string Address, string Email, string Phone, out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_USER.SaveUpdateUser";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_UserName", UserName, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Password", Password, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", Name, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Company", Company, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Address", Address, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Email", Email, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Phone", Phone, OracleMappingType.Decimal, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public void SaveUpdatePass(string UserName, string PassNew, out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_USER.SaveUpdatePass";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_UserName", UserName, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_PassNew", PassNew, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public async Task<UserViewModel> Authenticate(string UserName, string Password)
        {
            //var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
            ////client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);

            //var requestContent = new MultipartFormDataContent();


            //  //  byte[] data;

            ////}

            //requestContent.Add(new StringContent(request.USER_NAME.ToString()), "username");
            //requestContent.Add(new StringContent(request.PASSWORD.ToString()), "password");

            // var data = await client.PostAsync<PagedResult<UserViewModel>>($"/api/products/", requestContent);
            //return data; 



            //var json = JsonConvert.SerializeObject(request);
            //var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            //var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri(_configuration["BaseAddress"]);
            //var response = await client.PostAsync("/api/User/Get", httpContent);
            //if (response.IsSuccessStatusCode)
            //{
            //    return JsonConvert.DeserializeObject<ApiSuccessResult<string>>(await response.Content.ReadAsStringAsync());
            //}

            //return JsonConvert.DeserializeObject<ApiErrorResult<string>>(await response.Content.ReadAsStringAsync());
            try {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration[SystemConstants.AppSettings.BaseAddress]);
                var response = await client.GetAsync($"/api/User/{UserName}/{Password}");
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    UserViewModel myDeserializedObjList = (UserViewModel)JsonConvert.DeserializeObject(body,
                        typeof(UserViewModel));

                    return myDeserializedObjList;
                }
                return JsonConvert.DeserializeObject<UserViewModel>(body);
            }
            catch(Exception ex)
            {
                return null;
            }
          
        }

        //public async Task<UserViewModel> GetById(string UserName, string Password)
        //{
        //    var data = await GetAsync<UserViewModel>($"/api/User/Get/{UserName}/{Password}");

        //    return data;
        //}
    }
}
