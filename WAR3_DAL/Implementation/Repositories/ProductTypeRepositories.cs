﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class ProductTypeRepositories : IProductTypeRepositories
    {
        private readonly string ConnectedString;
        public ProductTypeRepositories(IConfiguration _configuration)
        {
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public PagedResult<ProductTypeViewModel> GetAllPaging(string keyword, int page, int pageSize, out bool result, out string message)
        {
            //try
            //{
            result = true;
            message = string.Empty;
            List<ProductTypeViewModel> data = new List<ProductTypeViewModel>();

            //    //var connection = new OracleConnection(ConnectedString);
            //    //var parameters = new OracleDynamicParameters();
            //    //parameters.Add("RETURN_VALUE", string.Empty, OracleMappingType.Varchar2, ParameterDirection.ReturnValue);
            //    //parameters.Add("PARAMETER1", parametervalue1, OracleDbType.Varchar2, ParameterDirection.Input);
            //    //parameters.Add("PARAMETER2", parametervalue1, OracleDbType.Xml, ParameterDirection.Input);

            //    //connection.Execute("Schema.Package.MyStoredProcedure", parameters, commandType: CommandType.StoredProcedure);
            //    //SqlMapper.Query<Customer>(con, "GetAllCustomer", commandTypeStoredProcedure).ToList();


            //    using (OracleConnection con = new OracleConnection(ConnectedString))
            //    {
            //        using (OracleCommand cmd = new OracleCommand())
            //        {
            //            con.Open();
            //            cmd.Connection = con;
            //            cmd.CommandText = "PKG_WR_CATEGORY.GetUserByName";
            //            cmd.CommandType = CommandType.StoredProcedure;
            //            cmd.Parameters.Add("EMP_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
            //            cmd.Parameters.Add("Error", OracleDbType.NVarchar2, ParameterDirection.Output);

            //            cmd.Parameters.Add("v_Page", OracleDbType.Decimal, ParameterDirection.Input).Value = page;
            //            cmd.Parameters.Add("v_PageSize", OracleDbType.Decimal, ParameterDirection.Input).Value = pageSize;



            //            data =SqlMapper.Query<ProductTypeViewModel>(con, cmd.CommandText, commandType: CommandType.StoredProcedure).ToList();

            //            cmd.ExecuteNonQuery();
            //            //data=SqlMapper.Query<ProductTypeViewModel>(con, "GetAllCustomer", commandTypeText).ToList();
            //            //data = con.Query<ProductTypeViewModel>().ToList();
            //            var paginationSet = new PagedResult<ProductTypeViewModel>()
            //            {
            //                Results = data,
            //                CurrentPage = page,
            //                RowCount = data[0].ROW_NUM,
            //                PageSize = pageSize
            //            };
            //            return paginationSet;
            //        }


            //    }
            //}
            //catch (Exception ex)
            //{
            //    error = false;
            //    messing = ex.Message;
            //    return null;
            //}



            //con.Open();
            //OracleCommand cmd = new OracleCommand();
            //cmd.CommandText = "Select * from WR_USER";
            //cmd.Connection = con;
            //cmd.CommandType = CommandType.Text;
            //OracleDataReader dr = cmd.ExecuteReader();
            //dr.Read();

            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.GetUserByName";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Keyword", keyword, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Page", page, OracleMappingType.Decimal, ParameterDirection.Input);
                dynamicParameters.Add("v_PageSize", pageSize, OracleMappingType.Decimal, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                dynamicParameters.Add("DATA", null, OracleMappingType.RefCursor, ParameterDirection.ReturnValue);

                try
                {
                    data = SqlMapper.Query<ProductTypeViewModel>(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (data.Count == 0 && result == true)
                        message = "NoData";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }

                oracleConnection.Close();
                PagedResult<ProductTypeViewModel> paginationSet = new PagedResult<ProductTypeViewModel>();
                if (result == true && message == "SUCCESS")
                {
                    paginationSet.Results = data;
                    paginationSet.CurrentPage = page;
                    paginationSet.RowCount = data[0].ROW_NUM;
                    paginationSet.PageSize = pageSize;
                }
                else if (result == true && message == "NoData")
                {
                    paginationSet.CurrentPage = page;
                    paginationSet.PageSize = pageSize;
                }
                //var paginationSet = new PagedResult<ProductTypeViewModel>()
                //{
                //    Results = data,
                //    CurrentPage = page,
                //    RowCount = data[0].ROW_NUM,
                //    PageSize = pageSize
                //};
                return paginationSet;
            }
        }

        public void SaveEntity(string Id, string Code, string Name, out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.SaveProductType";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Id", Id, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Code", Code, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", Name, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                     SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public List<ProductTypeViewModel> GetProductTypeById(string id, out bool result, out string message)
        {
            //try
            //{
            result = true;
            message = string.Empty;
            List<ProductTypeViewModel> data = new List<ProductTypeViewModel>();

            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.GetProductTypeById";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Id", id, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                dynamicParameters.Add("DATA", null, OracleMappingType.RefCursor, ParameterDirection.ReturnValue);

                try
                {
                    data = SqlMapper.Query<ProductTypeViewModel>(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (data.Count == 0 && result == true)
                        message = "NoData";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }

                oracleConnection.Close();
               
                return data;
            }
        }

        public void UpdateEntity(string Id, string Code, string Name, out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.UpdateProductType";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Id", Id, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Code", Code, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", Name, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public void DeleteEntity(string Id,out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.DeleteProductType";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Id", Id, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public List<ProductTypeViewModel> ImportExcel(string filePath, out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            try
            {
                int j = 0;
                int a = 0;
                int b = 0;
                List<ProductTypeViewModel> data = new List<ProductTypeViewModel>();
                using (var package = new ExcelPackage(new FileInfo(filePath)))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    if(workSheet.Cells[1, 1].Value.ToString().Trim()!="ID" || workSheet.Cells[1, 2].Value.ToString().Trim() != "NAME"|| workSheet.Cells[1, 3].Value.ToString().Trim() != "STATUS")
                    {
                        result = false;
                        message = "Template không đúng định dạng";
                        return null;
                    }
                    else
                    {
                        ProductTypeViewModel product;
                        a = workSheet.Dimension.End.Row;
                        for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                        {
                            product = new ProductTypeViewModel();

                            product.PT_ID = workSheet.Cells[i, 1].Value.ToString();

                            product.PT_NAME = workSheet.Cells[i, 2].Value.ToString();

                            int.TryParse(workSheet.Cells[i, 3].Value.ToString(), out var originalPrice);
                            product.PT_STATUS = originalPrice;

                            AddImportExxcel(product, out result, out message);
                            if (result == false)
                            {
                                data.Add(
        new ProductTypeViewModel()
        {
            SO_DONG = i,
            LOI = message,
            PT_ID = product.PT_ID,
            PT_NAME = product.PT_NAME,
        });
                            }
                            else
                            {
                                j = j + 1;
                            }
                            //_productRepository.Add(product);
                        }
                    }
                   
                }
                if (a != j)
                {
                    result = true;
                    message = j.ToString();
                }
                else
                {
                    message = message+j.ToString();
                }
                return data;
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
                return null;
            }
        }
        public void AddImportExxcel(ProductTypeViewModel product,  out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.IUProductType";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Id", product.PT_ID, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", product.PT_NAME, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Status", product.PT_STATUS, OracleMappingType.Decimal, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

        public List<ProductTypeViewModel> GetProductType( out bool result, out string message)
        {
            //try
            //{
            result = true;
            message = string.Empty;
            List<ProductTypeViewModel> data = new List<ProductTypeViewModel>();


            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.GetProductType";
                var dynamicParameters = new OracleDynamicParameters();
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                dynamicParameters.Add("DATA", null, OracleMappingType.RefCursor, ParameterDirection.ReturnValue);

                try
                {
                    data = SqlMapper.Query<ProductTypeViewModel>(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure).ToList();
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (data.Count == 0 && result == true)
                        message = "NoData";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }

                oracleConnection.Close();
              
                return data;
            }
        }
    }
}
