﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class PalletRepositories : BaseApiClient,IPalletRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public PalletRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<PalletViewModel>> GetAllPallet(string status, string fromDate, string toDate, string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<PalletViewModel>>(
                 $"api/Pallet/GetPalletPage?pageIndex={page}" +
                $"&pageSize={pageSize}" +
                $"&keyword={keyword}" +
                $"&status={status}" +
                $"&fromDate={fromDate}" +
                $"&toDate={toDate}");
            return data;
        }

        public async Task<PagedResultAPI<PalletViewModel>> SavePallet(string UserName, int number, string des)
        {
            PagedResultAPI<PalletViewModel> data1 = new PagedResultAPI<PalletViewModel>();
            try {
                PalletViewModel data = new PalletViewModel();

                data.NUMBER_CODE = number;
                data.DESCRIPTION = des;
                data.CREATE_USER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<PalletViewModel>>(
                 $"api/Pallet/SavePallet", json);
            }
            catch(Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }
           
            return data1;
        }

        public async Task<PagedResultAPI<PalletViewModel>> GetPalletById(string id)
        {
            var data = await GetAsync<PagedResultAPI<PalletViewModel>>(
              $"api/Pallet/GetPalletById?id={id}");
            return data;
        }

        public async Task<PagedResultAPI<PalletViewModel>> UpdatePallet(string UserName, string code, string des)
        {
            PalletViewModel data = new PalletViewModel();

            data.CODE = code;
            data.DESCRIPTION = des;
            data.CREATE_USER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<PalletViewModel>>(
             $"api/Pallet/UpdatePallet", json);
            return data1;
        }

        public async Task<PagedResultAPI<PalletViewModel>> DeletePallet(string UserName, string Id)
        {
            PalletViewModel data = new PalletViewModel();

            data.CODE = Id;
            data.CREATE_USER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<PalletViewModel>>(
             $"api/Pallet/DeletePallet", json);
            return data1;
        }

        public async Task<PagedResultAPI<PalletViewModel>> LoadListDetail(string id)
        {
            PagedResultAPI<PalletViewModel> data1 = new PagedResultAPI<PalletViewModel>();
            try
            {
                PalletViewModel data = new PalletViewModel();

                data.CODE = id;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<PalletViewModel>>(
                 $"api/Pallet/LoadListDetail", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<ProductViewModel>> ImportExcel(string filePath)
        {
            PagedResultAPI<ProductViewModel> data = new PagedResultAPI<ProductViewModel>();
            List<ProductViewModel> dataItem = new List<ProductViewModel>();
            data.Success = true;
            try
            {
                int j = 0;
                int a = 0;
                var message = "";
                using (var package = new ExcelPackage(new FileInfo(filePath)))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                    if(workSheet.Cells[1, 1].Value.ToString().Trim()!= "PRODUCT_TYPE" || workSheet.Cells[1, 2].Value.ToString().Trim() != "CODE" || workSheet.Cells[1, 3].Value.ToString().Trim() != "MODEL" || workSheet.Cells[1, 4].Value.ToString().Trim() != "NAME" || workSheet.Cells[1,5].Value.ToString().Trim() != "NAME_VN" || workSheet.Cells[1, 6].Value.ToString().Trim() != "CONTACT" || workSheet.Cells[1, 7].Value.ToString().Trim() != "DESCRIPTION" || workSheet.Cells[1, 8].Value.ToString().Trim() != "STATUS")
                    {
                        data.Success = false;
                        data.Message = "Template không đúng định dạng";
                        return data;
                    }
                    else
                    {
                        ProductViewModel product;
                        a = workSheet.Dimension.End.Row-1;
                        for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                        {
                            product = new ProductViewModel();
                            product.WR_PR_TYPE = workSheet.Cells[i, 1].Value == null ? string.Empty : workSheet.Cells[i, 1].Value.ToString();
                            product.WR_PR_CODE = workSheet.Cells[i, 2].Value == null ? string.Empty : workSheet.Cells[i, 2].Value.ToString();
                            product.WR_PR_MODEL = workSheet.Cells[i, 3].Value == null ? string.Empty : workSheet.Cells[i, 3].Value.ToString();
                            product.WR_PR_NAME =workSheet.Cells[i, 4].Value == null ? string.Empty : workSheet.Cells[i, 4].Value.ToString();
                            product.WR_PR_NAME_VN = workSheet.Cells[i, 5].Value == null ? string.Empty : workSheet.Cells[i, 5].Value.ToString();
                            product.WR_PR_CONTACT = workSheet.Cells[i, 6].Value == null ? string.Empty : workSheet.Cells[i, 6].Value.ToString();
                            product.WR_PR_DESCRIPTION = workSheet.Cells[i, 7].Value == null ? string.Empty : workSheet.Cells[i, 7].Value.ToString();
                            product.WR_PR_STATUS = workSheet.Cells[i, 8].Value == null ? string.Empty : workSheet.Cells[i, 8].Value.ToString();
                            //if (workSheet.Cells[i, 8].Value != null)
                            //{
                            //    product.WR_PR_STATUS = (Int32)workSheet.Cells[i, 8].Value;
                            //}
                           // product.WR_STATUS = workSheet.Cells[i, 8].Value == null ? string.Empty : workSheet.Cells[i, 8].Value.ToString();

                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 1].Value.ToString()))
                            //{
                            //    product.WR_PR_TYPE = workSheet.Cells[i, 1].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 2].Value.ToString()))
                            //{
                            //    product.WR_PR_CODE = workSheet.Cells[i, 2].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 3].Value.ToString()))
                            //{
                            //    product.WR_PR_MODEL = workSheet.Cells[i, 3].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 4].Value.ToString()))
                            //{
                            //    product.WR_PR_NAME = workSheet.Cells[i, 4].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 5].Value.ToString()))
                            //{
                            //    product.WR_PR_NAME_VN = workSheet.Cells[i, 5].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 6].Value.ToString()))
                            //{
                            //    product.WR_PR_CONTACT = workSheet.Cells[i, 6].Value.ToString();
                            //}
                            //if (String.IsNullOrEmpty(workSheet.Cells[i, 7].Value.ToString()))
                            //{
                            //    product.WR_PR_DESCRIPTION = workSheet.Cells[i, 7].Value.ToString();
                            //}
                            //AddImportExxcel(product, out result, out message);
                            var json = JsonConvert.SerializeObject(product);

                            var data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                             $"api/Product/IUProduct", json);
                            message = data1.Message;
                            if (data1.Success == false)
                            {
                                dataItem.Add(
        new ProductViewModel()
        {
            SO_DONG = i,
            LOI = data1.Message,
            WR_PR_TYPE = product.WR_PR_TYPE,
            WR_PR_CODE = product.WR_PR_CODE,
            WR_PR_MODEL = product.WR_PR_MODEL,
            WR_PR_NAME = product.WR_PR_NAME,
            WR_PR_NAME_VN = product.WR_PR_NAME_VN,
            WR_PR_CONTACT= product.WR_PR_CONTACT,
            WR_PR_DESCRIPTION = product.WR_PR_DESCRIPTION
        });
                            }
                            else
                            {
                                j = j + 1;
                            }
                            //_productRepository.Add(product);
                        }
                    }
                   
                }
                if (a != j)
                {
                    data.Items = dataItem;
                    data.Success = true;
                    data.Message = j.ToString();
                }
                else
                {
                    data.Message = message+j.ToString();
                }
                return data;
            }
            catch (Exception ex)
            {
                data.Success = false;
                data.Message = ex.Message;
            //result = false;
                //message = ex.Message;
                return data;
            }
        }
        public void AddImportExxcel(ProductViewModel product,  out bool result, out string message)
        {
            result = true;
            message = string.Empty;
            using (var oracleConnection = new OracleConnection(ConnectedString))
            {
                oracleConnection.Open();
                string query = "PKG_WR_CATEGORY.IUProduct";
                var dynamicParameters = new OracleDynamicParameters();
                //dynamicParameters.Add("userName", userName, OracleMappingType.Varchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Type", product.WR_PR_TYPE, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Code", product.WR_PR_CODE, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Model", product.WR_PR_MODEL, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name", product.WR_PR_NAME, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Name_VN", product.WR_PR_NAME_VN, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Contact", product.WR_PR_CONTACT, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Des", product.WR_PR_DESCRIPTION, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("v_Status", product.WR_STATUS, OracleMappingType.NVarchar2, ParameterDirection.Input);
                dynamicParameters.Add("vError", message, OracleMappingType.Varchar2, ParameterDirection.Output);
                try
                {
                    //oracleConnection();
                    //oracleConnection.Execute( query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    SqlMapper.Query(oracleConnection, query, param: dynamicParameters, commandType: CommandType.StoredProcedure);
                    message = dynamicParameters.GetParameter("vError").Value.ToString();
                    if (message == "SUCCESS")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    result = false;
                }
            }
        }

      
        public async Task<PagedResultAPI<PalletViewModel>> ExportExcel(string status, string fromDate, string toDate, string keyword)
        {
            var data=await GetAsync<PagedResultAPI<PalletViewModel>>(
              $"api/Pallet/GetAllPalletNoPage?keyword={keyword}" +
                $"&status={status}" +
                $"&fromDate={fromDate}" +
                $"&toDate={toDate}");
            return data;
        }
    }
}
