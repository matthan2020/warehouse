﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class ContactRepositories : BaseApiClient,IContactRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public ContactRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<ContactViewModel>> GetAllContact(string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<ContactViewModel>>(
                $"api/Contact/GetAllContactPage?pageIndex={page}" +
                $"&pageSize={pageSize}" +
                $"&keyword={keyword}");
            return data;
        }

        public async Task<PagedResultAPI<ContactViewModel>> SaveContact(string UserName,string type, string code, string name, string company, string address, string email, string phone, string receiverUser, string receiverDate, string des, string createDate)
        {
            PagedResultAPI<ContactViewModel> data1 = new PagedResultAPI<ContactViewModel>();
            try
            {
                ContactViewModel data = new ContactViewModel();

                data.CT_CODE = code;
                data.CT_NAME = name;
                data.CT_TYPE = type;
                data.CT_COMPANY = company;
                data.CT_ADDRESS = address;
                data.CT_EMAIL = email;
                data.CT_PHONE = phone;
                data.CT_RECEIVER_USER = receiverUser;
                data.CT_RECEIVER_DATE = receiverDate;
                data.CT_DESCRIPTION = des;
                data.CT_CREATEDATE = createDate;
                data.CT_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<ContactViewModel>>(
                 $"api/Contact/SaveContact", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<ContactViewModel>> GetContactById(string id)
        {
            var data = await GetAsync<PagedResultAPI<ContactViewModel>>(
                $"api/Contact/GetContactById?id={id}");
            return data;
        }

        public async Task<PagedResultAPI<ContactViewModel>> UpdateContact(string type, string code, string name, string company, string address, string email, string phone, string receiverUser, string receiverDate, string des, string createDate,string UserName)
        {
            ContactViewModel data = new ContactViewModel();

            data.CT_CODE = code;
            data.CT_NAME = name;
            data.CT_TYPE = type;
            data.CT_COMPANY = company;
            data.CT_ADDRESS = address;
            data.CT_EMAIL = email;
            data.CT_PHONE = phone;
            data.CT_RECEIVER_USER = receiverUser;
            data.CT_RECEIVER_DATE = receiverDate;
            data.CT_DESCRIPTION = des;
            data.CT_CREATEDATE = createDate;
            data.CT_UPDATEUSER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<ContactViewModel>>(
             $"api/Contact/UpdateContact", json);
            return data1;
        }

        public async Task<PagedResultAPI<ContactViewModel>> DeleteContact(string UserName,string Id)
        {
            ContactViewModel data = new ContactViewModel();

            data.CT_CODE = Id;
            data.CT_UPDATEUSER = UserName;
            var json = JsonConvert.SerializeObject(data);

            var data1 = await PostAsync<PagedResultAPI<ContactViewModel>>(
             $"api/Contact/DeleteContact", json);
            return data1;
        }




        public async Task<PagedResultAPI<ContactViewModel>> ExportExcel(string keyword)
        {
            var data = await GetAsync<PagedResultAPI<ContactViewModel>>(
               $"api/Contact/GetAllContactNoPage?keyword={keyword}");
            return data;
        }
    }
}
