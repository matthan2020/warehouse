﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Entities;

namespace WAR3_DAL.Implementation.IRepositories
{
    public class CustomClaimsPrincipalFactory : UserClaimsPrincipalFactory<User>
    {
        UserManager<User> _userManger;

        public CustomClaimsPrincipalFactory(UserManager<User> userManager, IOptions<IdentityOptions> options)
            : base(userManager, options)
        {
            _userManger = userManager;
        }

        public async override Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var principal = await base.CreateAsync(user);
            //var roles = await _userManger.GetRolesAsync(user);
            ((ClaimsIdentity)principal.Identity).AddClaims(new[]
            {
                new Claim("UserName",user.USER_NAME),
                new Claim("Email",user.EMAIL),
                new Claim("FullName",user.NAME),
                new Claim("PassWord",user.PASSWORD)
                //new Claim("Role",string.Join(";",roles))
            });
            return principal;
        }
    }
}
