﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class ImportGoodsRepositories : BaseApiClient,IImportGoodsRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public ImportGoodsRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<ImportGoodsViewModel>> GetAllImportGoods(string status, string fromDate, string toDate, string keyword, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<ImportGoodsViewModel>>(
                $"api/ImportGoods/GetAllImportGoodsPage?pageIndex={page}" +
                $"&pageSize={pageSize}" +
                $"&keyword={keyword}"+
                $"&status={status}" +
                $"&fromDate={fromDate}" +
                $"&toDate={toDate}" );
            return data;
        }

        public async Task<PagedResultAPI<ImportGoodsViewModel>> SaveImportGoods(string UserName,string licensePlates, string licenseName, string fromDate, string des,string filePath)
        {
            if(filePath==null&& filePath == "")
            {
                PagedResultAPI<ImportGoodsViewModel> data1 = new PagedResultAPI<ImportGoodsViewModel>();
                try
                {
                    ImportGoodsViewModel data = new ImportGoodsViewModel();

                    data.IG_LICENSE_PLATES = licensePlates;
                    data.IG_LICENSE_NAME = licenseName;
                    data.IG_GOODS_DATE = fromDate;
                    data.IG_FILE = filePath;
                    data.IG_DESCRIPTION = des;
                    data.IG_CREATEUSER = UserName;
                    var json = JsonConvert.SerializeObject(data);

                    //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                    //             $"/api/Product/IUProduct", json);
                    data1 = await PostAsync<PagedResultAPI<ImportGoodsViewModel>>(
                     $"api/ImportGoods/SaveImportGoods", json);
                }
                catch (Exception ex)
                {
                    data1.Success = false;
                    data1.Message = ex.Message;
                }

                return data1;
            }
            else
            {
                PagedResultAPI<ImportGoodsViewModel> data1 = new PagedResultAPI<ImportGoodsViewModel>();
                try
                {
                    ImportGoodsViewModel data = new ImportGoodsViewModel();

                    data.IG_LICENSE_PLATES = licensePlates;
                    data.IG_LICENSE_NAME = licenseName;
                    data.IG_GOODS_DATE = fromDate;
                    data.IG_FILE = filePath.Split("\\")[filePath.Split("\\").Length-1];
                    data.IG_DESCRIPTION = des;
                    data.IG_CREATEUSER = UserName;
                    var json = JsonConvert.SerializeObject(data);

                    //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                    //             $"/api/Product/IUProduct", json);
                    data1 = await PostAsync<PagedResultAPI<ImportGoodsViewModel>>(
                     $"api/ImportGoods/SaveImportGoods", json);
                }
                catch (Exception ex)
                {
                    data1.Success = false;
                    data1.Message = ex.Message;
                }
                if (data1.Success == true)
                {
                    PagedResultAPI<ImportGoodsViewModel> data = new PagedResultAPI<ImportGoodsViewModel>();
                    List<ImportGoodsViewModel> dataItem = new List<ImportGoodsViewModel>();
                    data.Success = true;
                    try
                    {
                        int j = 0;
                        int a = 0;
                        var message = "";
                        using (var package = new ExcelPackage(new FileInfo(filePath)))
                        {
                            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                            if (workSheet.Cells[1, 1].Value.ToString().Trim() != "Ma_san_pham" || workSheet.Cells[1, 2].Value.ToString().Trim() != "So_luong" || workSheet.Cells[1, 3].Value.ToString().Trim() != "Ghi_chu")
                            {
                                data.Success = false;
                                data.Message = "Template không đúng định dạng";
                                return data;
                            }
                            else
                            {
                                MapProductGoodsViewModel product;
                                a = workSheet.Dimension.End.Row - 1;
                                for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                                {
                                    product = new MapProductGoodsViewModel();
                                    product.MPG_GOODS = data1.Id;
                                    product.MPG_PRODUCT = workSheet.Cells[i, 1].Value == null ? string.Empty : workSheet.Cells[i, 1].Value.ToString();
                                    product.MPG_NUMBER = Convert.ToInt32(workSheet.Cells[i, 2].Value);
                                    product.MPG_DESCRIPTION = workSheet.Cells[i, 3].Value == null ? string.Empty : workSheet.Cells[i, 3].Value.ToString();
                                    product.MPG_CREATEUSER = UserName;
                                    if (workSheet.Cells[i, 1].Value == null)
                                    {
                                        dataItem.Add(
                    new ImportGoodsViewModel()
                    {
                        SO_DONG = i,
                        LOI = "Mã sản phẩm không được để trống",
                        IG_LICENSE_PLATES = product.MPG_PRODUCT,
                        IG_ID = product.MPG_NUMBER,
                        IG_DESCRIPTION = product.MPG_DESCRIPTION
                    });
                                    }
                                    else
                                    {
                                        var json = JsonConvert.SerializeObject(product);

                                        var dataList = await PostAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                                         $"api/ImportGoods/SaveProductGoodsByImport", json);
                                        message = dataList.Message;
                                        if (dataList.Success == false)
                                        {
                                            dataItem.Add(
                    new ImportGoodsViewModel()
                    {
                        SO_DONG = i,
                        LOI = dataList.Message,
                        IG_LICENSE_PLATES = product.MPG_PRODUCT,
                        IG_ID = product.MPG_NUMBER,
                        IG_DESCRIPTION = product.MPG_DESCRIPTION
                    });
                                        }
                                        else
                                        {
                                            j = j + 1;
                                        }
                                    }
                                   
                                    //_productRepository.Add(product);
                                }
                            }

                        }
                        if (a != j)
                        {
                            data.Items = dataItem;
                            data.Success = true;
                            data.Message = j.ToString();
                            await GetAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                            $"api/ImportGoods/DeleteMapProductGoodsImport?id={data1.Id}");
                        }
                        else
                        {
                            data.Message = message + j.ToString();
                            data.Id = data1.Id;
                        }
                        return data;
                    }
                    catch (Exception ex)
                    {
                        data.Success = false;
                        data.Message = ex.Message;
                        await GetAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                           $"api/ImportGoods/DeleteMapProductGoodsImport?id={data1.Id}");
                        //result = false;
                        //message = ex.Message;
                        return data;
                    }
                }
                else
                {
                    return data1;
                }
               
            }
            
        }

        public async Task<PagedResultAPI<MapProductGoodsViewModel>> SaveImportGoodsProduct(string UserName, int IdGoods, string ProductCode, int soLuong)
        {
            PagedResultAPI<MapProductGoodsViewModel> data1 = new PagedResultAPI<MapProductGoodsViewModel>();
            try
            {
                MapProductGoodsViewModel data = new MapProductGoodsViewModel();

                data.MPG_GOODS = IdGoods;
                data.MPG_PRODUCT = ProductCode;
                data.MPG_NUMBER = soLuong;
                data.MPG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                 $"api/ImportGoods/SaveImportGoodsProduct", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<MapProductGoodsViewModel>> GetAllMapGoodsProduct(string idGoods, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                 $"api/ImportGoods/GetAllMapGoodsProductPage?pageIndex={page}" +
                 $"&pageSize={pageSize}" +
                 $"&idGoods={idGoods}");
            return data;
        }
        public async Task<PagedResultAPI<ImportGoodsViewModel>> GetAllGoodsById(string idGoods)
        {
            var data = await GetAsync<PagedResultAPI<ImportGoodsViewModel>>(
                 $"api/ImportGoods/GetAllGoodsById?id={idGoods}");
            return data;
        }

        public async Task<PagedResultAPI<MapPalletProductViewModel>> GetAllMapGoodsPallet(string idGoods, int page, int pageSize)
        {
            var data = await GetAsync<PagedResultAPI<MapPalletProductViewModel>>(
                  $"api/ImportGoods/GetAllMapGoodsPalletPage?pageIndex={page}" +
                 $"&pageSize={pageSize}" +
                 $"&idGoods={idGoods}");
            return data;
        }

        public async Task<PagedResultAPI<MapProductGoodsViewModel>> GetProductByGoods(string idGoods)
        {
            var data = await GetAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                $"api/ImportGoods/GetProductByGoods?idGoods={idGoods}");
            return data;
        }

        public async Task<PagedResultAPI<Pallet>> GetPallet( )
        {
            var data = await GetAsync<PagedResultAPI<Pallet>>(
                $"api/ImportGoods/GetPallet");
            return data;
        }
        public async Task<PagedResultAPI<MapPalletProduct>> SavePalletProduct(string UserName, int id, string CodePallet, string CodeProduct, int NumberPallet)
        {
            PagedResultAPI<MapPalletProduct> data1 = new PagedResultAPI<MapPalletProduct>();
            try
            {
                MapPalletProduct data = new MapPalletProduct();

                data.MPP_GOODS = id;
                data.MPP_PRODUCT = CodeProduct;
                data.MPP_PALLET = CodePallet;
                data.MPP_NUMBER = NumberPallet;
                data.MPP_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapPalletProduct>>(
                 $"api/ImportGoods/SavePalletProduct", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<MapPalletProductViewModel>> DeletePallet(string UserName, string Id)
        {
            PagedResultAPI<MapPalletProductViewModel> data1 = new PagedResultAPI<MapPalletProductViewModel>();
            try
            {
                MapPalletProductViewModel data = new MapPalletProductViewModel();

                data.ID_REMOVE = Id;
                data.MPP_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapPalletProductViewModel>>(
                 $"api/ImportGoods/DeletePalletInGoods", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<ImportGoodsViewModel>> UpdateImportGoods(string UserName,int idGoods, string licensePlates, string licenseName, string fromDate, string des)
        {
            PagedResultAPI<ImportGoodsViewModel> data1 = new PagedResultAPI<ImportGoodsViewModel>();
            try
            {
                ImportGoodsViewModel data = new ImportGoodsViewModel();

                data.IG_LICENSE_PLATES = licensePlates;
                data.IG_LICENSE_NAME = licenseName;
                data.IG_GOODS_DATE = fromDate;
                data.IG_ID = idGoods;
                data.IG_DESCRIPTION = des;
                data.IG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<ImportGoodsViewModel>>(
                 $"api/ImportGoods/UpdateImportGoods", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<MapProductGoodsViewModel>> DeleteProduct(string UserName, string Id,string IdProduct)
        {
            PagedResultAPI<MapProductGoodsViewModel> data1 = new PagedResultAPI<MapProductGoodsViewModel>();
            try
            {
                MapProductGoodsViewModel data = new MapProductGoodsViewModel();

                data.ID_REMOVE = Id;
                data.MPG_PRODUCT = IdProduct;
                data.MPG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                 $"api/ImportGoods/DeleteProductInGoods", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<MapProductGoodsViewModel>> GetMapProducttById(string id, string codeProducct)
        {
            var data = await GetAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                $"api/ImportGoods/GetMapProducttById?id={id}"+
                $"&product={codeProducct}");
            return data;
        }


        public async Task<PagedResultAPI<MapProductGoodsViewModel>> SaveMapProduct(string UserName,int id, string codeProduct, int so_luong)
        {
            PagedResultAPI<MapProductGoodsViewModel> data1 = new PagedResultAPI<MapProductGoodsViewModel>();
            try
            {
                MapProductGoodsViewModel data = new MapProductGoodsViewModel();

                data.MPG_GOODS = id;
                data.MPG_PRODUCT = codeProduct;
                data.MPG_NUMBER = so_luong;
                data.MPG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                 $"api/ImportGoods/SaveMapProduct", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<MapProductGoodsViewModel>> UpdateMapProduct(string UserName, int id, string codeProductCu, string codeProduct, int so_luong)
        {
            PagedResultAPI<MapProductGoodsViewModel> data1 = new PagedResultAPI<MapProductGoodsViewModel>();
            try
            {
                MapProductGoodsViewModel data = new MapProductGoodsViewModel();

                data.MPG_GOODS = id;
                data.MPG_PRODUCT = codeProduct;
                data.MPG_PRODUCT_OTHER = codeProductCu;
                data.MPG_NUMBER = so_luong;
                data.MPG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapProductGoodsViewModel>>(
                 $"api/ImportGoods/UpdateMapProduct", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<MapPalletProductViewModel>> GetMapPalletById(string id)
        {
            var data = await GetAsync<PagedResultAPI<MapPalletProductViewModel>>(
                $"api/ImportGoods/GetMapPalletById?id={id}");
            return data;
        }

        public async Task<PagedResultAPI<MapPalletProductViewModel>> UpdateMapPallet(string UserName, int id, int idPallet, string CodePallet, string CodeProduct, int NumberPallet, string checkcapnhat )
        {
            PagedResultAPI<MapPalletProductViewModel> data1 = new PagedResultAPI<MapPalletProductViewModel>();
            try
            {
                MapPalletProductViewModel data = new MapPalletProductViewModel();

                data.MPP_GOODS = id;
                data.MPP_ID = idPallet;
                data.MPP_PALLET = CodePallet;
                data.MPP_PRODUCT = CodeProduct;
                data.MPP_NUMBER = NumberPallet;
                data.MPP_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<MapPalletProductViewModel>>(
                 $"api/ImportGoods/UpdateMapPallet", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<ImportGoodsViewModel>> DeleteImportGoods(string UserName,string Id)
        {
            PagedResultAPI<ImportGoodsViewModel> data1 = new PagedResultAPI<ImportGoodsViewModel>();
            try
            {
                ImportGoodsViewModel data = new ImportGoodsViewModel();

                data.ID_REMOVE = Id;
                data.IG_CREATEUSER = UserName;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<ImportGoodsViewModel>>(
                 $"api/ImportGoods/DeleteImportGoods", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<ImportGoodsViewModel>> ExportExcel(string status, string fromDate, string toDate, string keyword)
        {
            var data = await GetAsync<PagedResultAPI<ImportGoodsViewModel>>(
                $"api/ImportGoods/GetAllImportGoodsNoPage?keyword={keyword}" +
                $"&status={status}" +
                $"&fromDate={fromDate}" +
                $"&toDate={toDate}");
            return data;
        }
    }
}
