﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IUserRepositories
    {
        List<UserViewModel> GetUserByName(string UserName, string Password, out bool result, out string message);

        void SaveUpdateUser(string UserName, string Password, string Name, string Company, string Address, string Email, string Phone, out bool result, out string message);

        void SaveUpdatePass(string UserName, string PassNew, out bool result, out string message);

        Task<UserViewModel> Authenticate(string UserName, string Password);

    }
}
