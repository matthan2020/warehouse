﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IStockingRepositories
    {
        Task<PagedResultAPI<MapPalletProductViewModel>> GetAllStocking( string fromDate, string toDate, string keyword, int page, int pageSize);

      

        Task<PagedResultAPI<MapPalletProductViewModel>> GetStockingById(string id);

        Task<PagedResultAPI<MapPalletProductViewModel>> UpdateStocking(string UserName, int id, string location);

      
    }
}
