﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IImportGoodsRepositories
    {
        Task<PagedResultAPI<ImportGoodsViewModel>> GetAllImportGoods(string status, string fromDate, string toDate, string keyword, int page, int pageSize);

        Task<PagedResultAPI<ImportGoodsViewModel>> SaveImportGoods(string UserName,string licensePlates, string licenseName, string fromDate, string des,string filePath);

        Task<PagedResultAPI<MapProductGoodsViewModel>> SaveImportGoodsProduct(string UserName, int IdGoods, string ProductCode, int soLuong);

        Task<PagedResultAPI<MapProductGoodsViewModel>> GetAllMapGoodsProduct(string idGoods,  int page, int pageSize);

        Task<PagedResultAPI<ImportGoodsViewModel>> GetAllGoodsById(string idGoods);

        Task<PagedResultAPI<MapPalletProductViewModel>> GetAllMapGoodsPallet(string idGoods, int page, int pageSize);

        Task<PagedResultAPI<MapProductGoodsViewModel>> GetProductByGoods(string idGoods);

        Task<PagedResultAPI<Pallet>> GetPallet();
        Task<PagedResultAPI<MapPalletProduct>> SavePalletProduct(string UserName, int id, string CodePallet, string CodeProduct, int NumberPallet);
        Task<PagedResultAPI<MapPalletProductViewModel>> DeletePallet(string UserName, string Id);

        Task<PagedResultAPI<ImportGoodsViewModel>> UpdateImportGoods(string UserName,int idGoods, string licensePlates, string licenseName, string fromDate, string des);
        Task<PagedResultAPI<MapProductGoodsViewModel>> DeleteProduct(string UserName, string Id,string IdProduct);




        Task<PagedResultAPI<MapProductGoodsViewModel>> GetMapProducttById(string id,string codeProducct);

        Task<PagedResultAPI<MapProductGoodsViewModel>> SaveMapProduct(string UserName,int id, string codeProduct, int so_luong);

        Task<PagedResultAPI<MapProductGoodsViewModel>> UpdateMapProduct(string UserName, int id, string codeProductCu,string codeProduct, int so_luong);
        Task<PagedResultAPI<MapPalletProductViewModel>> GetMapPalletById(string id);
        Task<PagedResultAPI<MapPalletProductViewModel>> UpdateMapPallet(string UserName, int id, int idPallet,string CodePallet, string CodeProduct, int NumberPallet,string checkcapnhat);

        Task<PagedResultAPI<ImportGoodsViewModel>> DeleteImportGoods(string UserName,string Id);







        Task<PagedResultAPI<ImportGoodsViewModel>> ExportExcel(string status, string fromDate, string toDate, string keyword);

    }
}
