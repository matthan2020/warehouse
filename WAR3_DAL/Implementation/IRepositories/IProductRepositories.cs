﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IProductRepositories
    {
        Task<PagedResultAPI<ProductViewModel>> GetAllProduct( string keyword, int page, int pageSize);

        Task<PagedResultAPI<ProductViewModel>> SaveProduct(ProductViewModel data);

        Task<PagedResultAPI<ProductViewModel>> GetProductById(string id);

        Task<PagedResultAPI<ProductViewModel>> UpdateProduct(string UserName, string productType, string code, string model, string name, string nameVN, string contact, string des);

        Task<PagedResultAPI<ProductViewModel>> DeleteProduct(string UserName, string Id);

        Task<PagedResultAPI<ProductViewModel>>ImportExcel(string filePath);

        Task<PagedResultAPI<ProductViewModel>> ExportExcel(string keyword);
    }
}
