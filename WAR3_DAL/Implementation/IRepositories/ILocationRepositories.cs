﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface ILocationRepositories
    {
        Task<PagedResultAPI<LocationViewModel>> GetAllLocation( string keyword, int page, int pageSize);

        Task<PagedResultAPI<LocationViewModel>> SaveLocation(string UserName, string code, string name);

        Task<PagedResultAPI<LocationViewModel>> GetLocationById(string id);

        Task<PagedResultAPI<LocationViewModel>> UpdateLocation(string UserName,string code,  string name);

        Task<PagedResultAPI<LocationViewModel>> DeleteLocation(string UserName, string Id);

        Task<PagedResultAPI<LocationViewModel>> ImportExcel(string filePath);

        Task<PagedResultAPI<LocationViewModel>> ExportExcel( string keyword);
    }
}
