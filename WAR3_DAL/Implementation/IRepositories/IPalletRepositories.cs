﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IPalletRepositories
    {
        Task<PagedResultAPI<PalletViewModel>> GetAllPallet(string status, string fromDate, string toDate, string keyword, int page, int pageSize);

        Task<PagedResultAPI<PalletViewModel>> SavePallet(string UserName,int number,string des);

        Task<PagedResultAPI<PalletViewModel>> GetPalletById(string id);

        Task<PagedResultAPI<PalletViewModel>> UpdatePallet(string UserName, string code, string des);

        Task<PagedResultAPI<PalletViewModel>> DeletePallet(string UserName, string Id);

        Task<PagedResultAPI<PalletViewModel>> LoadListDetail(string id);

        Task<PagedResultAPI<ProductViewModel>>ImportExcel(string filePath);

        Task<PagedResultAPI<PalletViewModel>> ExportExcel(string status, string fromDate, string toDate, string keyword);
    }
}
