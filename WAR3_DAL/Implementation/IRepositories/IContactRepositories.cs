﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IContactRepositories
    {
        Task<PagedResultAPI<ContactViewModel>> GetAllContact( string keyword, int page, int pageSize);

        Task<PagedResultAPI<ContactViewModel>> SaveContact(string UserName,string type, string code, string name, string company, string address, string email, string phone, string receiverUser, string receiverDate, string des, string createDate);

        Task<PagedResultAPI<ContactViewModel>> GetContactById(string id);

        Task<PagedResultAPI<ContactViewModel>> UpdateContact(string type, string code, string name, string company, string address, string email, string phone, string receiverUser, string receiverDate, string des, string createDate, string UsreName);

        Task<PagedResultAPI<ContactViewModel>> DeleteContact(string UsreName,string Id);


        Task<PagedResultAPI<ContactViewModel>> ExportExcel(string keyword);
    }
}
