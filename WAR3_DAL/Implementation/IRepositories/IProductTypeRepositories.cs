﻿using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IProductTypeRepositories
    {
        PagedResult<ProductTypeViewModel> GetAllPaging( string keyword, int page, int pageSize, out bool result, out string message);

        void SaveEntity(string Id, string Code, string Name, out bool result, out string message);

        List<ProductTypeViewModel> GetProductTypeById(string id, out bool result, out string message);

        void UpdateEntity(string Id, string Code, string Name, out bool result, out string message);

        void DeleteEntity(string Id, out bool result, out string message);

        List<ProductTypeViewModel> ImportExcel(string filePath, out bool result, out string message);

        List<ProductTypeViewModel> GetProductType( out bool result, out string message);
    }
}
