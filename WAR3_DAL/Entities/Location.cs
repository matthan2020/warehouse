﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Location
    {
        public string LC_CODE { set; get; }
        public string LC_NAME { set; get; }
        public string LC_UPDATEUSER { set; get; }
        public string LC_UPDATEDATE { set; get; }
        public string LC_CREATEDATE { set; get; }
        public string LC_CREATEUSER { set; get; }
        public string LC_STATUS { set; get; }
    }
}
