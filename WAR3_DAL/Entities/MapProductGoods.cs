﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class MapProductGoods
    {
        public int MPG_GOODS { set; get; }
        public string MPG_PRODUCT { set; get; }
        public int MPG_NUMBER { set; get; }
        public int MPG_REALITY { set; get; }
        
        public string MPG_DESCRIPTION { set; get; }
        public string MPG_STATUS { set; get; }
        public string MPG_CREATEUSER { set; get; }
        public string MPG_CREATEDATE { set; get; }
        public string MPG_PRODUCT_OTHER { set; get; }
    }
}
