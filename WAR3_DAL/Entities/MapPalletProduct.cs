﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class MapPalletProduct
    {
        public int MPP_ID { set; get; }
        public int MPP_GOODS { set; get; }
        public string MPP_PRODUCT { set; get; }
        public int MPP_NUMBER { set; get; }
        public int MPP_STATUS { set; get; }
        public string MPP_CREATEDATE { set; get; }
        public string MPP_CREATEUSER { set; get; }
        public string MPP_PALLET { set; get; }
        public string MPP_LOCATION { set; get; }
    }
}
