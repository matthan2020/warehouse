﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Contact
    {
        public string CT_CODE { set; get; }
        public string CT_TYPE { set; get; }
        public string CT_NAME { set; get; }
        public string CT_COMPANY { set; get; }
        public string CT_ADDRESS { set; get; }
        public string CT_PHONE { set; get; }
        public string CT_EMAIL { set; get; }
        public string CT_STATUS { set; get; }
        public string CT_RECEIVER_USER { set; get; }
        public string CT_RECEIVER_DATE { set; get; }
        public string CT_DESCRIPTION { set; get; }
        public string CT_CREATEDATE { set; get; }
        public string CT_CREATEUSER { set; get; }
        public string CT_UPDATEDATE { set; get; }
        public string CT_UPDATEUSER { set; get; }
    }
}
