﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Role
    {
        public string USER_NAME { set; get; }
        public string PASSWORD { set; get; }
        public string NAME { set; get; }
        public string NAME_COMPANY { set; get; }
        public string ADDRESS { set; get; }
        public string EMAIL { set; get; }
        public decimal PHONE { set; get; }
        public int STATUS { set; get; }
        public string CREATE_DATE { set; get; }
        public string CREATE_USER { set; get; }
    }
}
