﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class ProductType
    {
        public string PT_ID { set; get; }
        //public string PT_MODEL { set; get; }
        public string PT_NAME { set; get; }
        public int PT_STATUS { set; get; }
        public string PT_CREATE_DATE { set; get; }
        public string PT_CREATE_USER { set; get; }
    }
}
