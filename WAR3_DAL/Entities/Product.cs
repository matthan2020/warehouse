﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Product
    {
        public string WR_PR_TYPE { set; get; }
        public string WR_PR_CODE { set; get; }
        public string WR_PR_MODEL { set; get; }
        public string WR_PR_NAME { set; get; }
        public string WR_PR_NAME_VN { set; get; }
        public string WR_PR_CONTACT { set; get; }
        public string WR_PR_DESCRIPTION { set; get; }
        public string WR_PR_STATUS { set; get; }
        public string WR_PR_CREATEDATE { set; get; }
        public string WR_PR_CREATEUSER { set; get; }
    }
}
