﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class ImportGoods
    {
        public int IG_ID { set; get; }
        public string IG_LICENSE_PLATES { set; get; }
        public string IG_LICENSE_NAME { set; get; }
        public string IG_GOODS_DATE { set; get; }
        public int IG_STATUS { set; get; }
        public string IG_FILE { set; get; }
        public string IG_DESCRIPTION { set; get; }
        public string IG_CREATEDATE { set; get; }
        public string IG_CREATEUSER { set; get; }
    }
}
