﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Pallet
    {
        public string CODE { set; get; }
        public int STATUS { set; get; }
        public string CREATE_DATE { set; get; }
        public string CREATE_USER { set; get; }
        public string CODE_NAME { set; get; }
        public string DESCRIPTION { set; get; }
    }
}
