﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WAR3_DAL.Dtos
{
    public class PagedResultAPIOther
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public int id { get; set; }
        public string FileSave { get; set; }
    }
}
