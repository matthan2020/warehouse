﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class ImportGoodsViewModel : ImportGoods
    {
        public int ROW_NUM { get; set; }
        public string STATUS { get; set; }
        public int SO_DONG { get; set; }

        public string LOI { get; set; }

        public string ID_REMOVE { get; set; }

        public int PHAN_BIET { set; get; }
        public int SO_LUONG { set; get; }

    }
}
