﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class ContactViewModel : Contact
    {
        public int ROW_NUM { get; set; }
        public string PHONE_NUMBER { get; set; }

    }
}
