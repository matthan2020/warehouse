﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class GetProductTypeViewModel 
    {
        public string ID { set; get; }
        public string NAME { set; get; }
        public string CREATEUSER { set; get; }

    }
}
