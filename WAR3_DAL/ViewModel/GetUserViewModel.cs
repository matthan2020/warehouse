﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class GetUserViewModel 
    {
        public string USERNAME { set; get; }
        public string PASSWORD { set; get; }
        public string NAME { set; get; }
        public string NAMECOMPANY { set; get; }
        public string ADDRESS { set; get; }
        public string EMAIL { set; get; }
        public string PHONE { set; get; }
        public string CREATEUSER { set; get; }

    }
}
