﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class MapPalletProductViewModel : MapPalletProduct
    {
        public int ROW_NUM { get; set; }
        public string STATUS { get; set; }
        public string ID_REMOVE { get; set; }

    }
}
